from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_disabled_line_edit, get_disabled_button, get_center_label


class ItemNameEditorGui(QWidget):
    le_line = None
    pb_clear = None
    pb_save = None
    pb_reset = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.le_line = get_disabled_line_edit()
        self.pb_clear = get_disabled_button("Clear")
        self.pb_save = get_disabled_button("Save")
        self.pb_reset = get_disabled_button("Reset")
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        l_main.addWidget(self.le_line)
        l_buttons = QHBoxLayout()
        l_main.addLayout(l_buttons)
        l_main.addStretch(1)
        l_buttons.addWidget(self.pb_clear)
        l_buttons.addWidget(self.pb_save)
        l_buttons.addWidget(self.pb_reset)

