import os

from low_level import path_list
from low_level.common_word_list_controller import CommonWordListController
from low_level.grammar_list_controller import GrammarListController

# Singleton.
from low_level.tag_list_controller import TagListController
from low_level.training_controller import TrainingController
from low_level.user_word_list_controller import UserWordListController


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DataController(metaclass=Singleton):
    # Public variables and constants.
    common_word_list = None
    user_word_list = None
    tag_list = None
    grammar_list = None
    training = None

    # Private variables and constants.
    __instance = None
    __path_list = None

    # Private methods.
    def __init__(self):
        self.__path_list = path_list.path_list()
        if not os.path.isdir(self.__path_list.root_path):
            os.mkdir(self.__path_list.root_path)
        if not os.path.isdir(self.__path_list.lists_path):
            os.mkdir(self.__path_list.lists_path)
        self.tag_list = TagListController()
        self.common_word_list = CommonWordListController()
        self.user_word_list = UserWordListController(self.common_word_list)
        self.grammar_list = GrammarListController()
        self.training = TrainingController(self.common_word_list)

    @classmethod
    def __get_instance(cls):
        if not cls.__instance:
            cls.__instance = DataController()
        return cls.__instance
