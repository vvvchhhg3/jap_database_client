from low_level import path_list
from low_level.item_types import grammar_example_item, grammar_item
import xml.etree.ElementTree as ET


class GrammarListController:
    # Public methods.
    def get_list(self):
        return self.__grammars.copy()

    def read(self):
        f = open(self.__path_list.grammars_path, "r", encoding='utf8')
        f_data = f.read()
        f.close()
        root = ET.ElementTree(ET.fromstring(f_data)).getroot()
        self.__grammars = []
        for item in root.findall('item'):
            examples = []
            for example in item.findall('example'):
                examples.append(grammar_example_item(int(example.get('id')),
                                                     example.find('jp').text,
                                                     example.find('furigana').text))
            self.__grammars.append(grammar_item(int(item.get('id')), item.get('jp'), item.get('translate'), examples))

    # Private variables and constants.
    __path_list = None

    __grammars = None

    # Private methods.
    def __init__(self):
        self.__path_list = path_list.path_list()
        self.read()
