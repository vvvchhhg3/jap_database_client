import os

from low_level import path_list


class TagListController:
    def get_sorted_list(self):
        return sorted(self.__tags)

    def replace_tag(self, old, new):
        self.__tags[self.__tags.index(old)] = new

    def add_empty_tag(self):
        self.__tags.append("")

    def remove_tag(self, tag):
        self.__tags.remove(tag)

    def is_empty_tag_exist(self):
        return "" in self.__tags

    def is_tag_exist(self, tag):
        return tag in self.__tags

    def read(self):
        f = open(self.__path_list.tags_path, "r", encoding='utf8')
        self.__tags = []
        while True:
            line = f.readline()
            if not line:
                break
            tag = line.replace("\n", "")
            if len(tag) != 0:
                self.__tags.append(tag)
        f.close()

    def save(self):
        f = open(self.__path_list.tags_path, "w", encoding='utf8')
        for i in self.__tags:
            f.writelines(i + "\n")
        f.close()

    # Private variables and constants.
    __path_list = None

    __tags = None

    # Private methods.
    def __init__(self):
        self.__path_list = path_list.path_list()
        if os.path.isfile(self.__path_list.tags_path):
            self.read()
        else:
            self.__tags = []
            self.save()
