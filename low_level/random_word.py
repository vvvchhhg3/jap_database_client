import random

from PyQt5.QtCore import QObject


class RandomWord(QObject):
    # Public methods.
    def set_words(self, words):
        self.__words = words

    def set_tag(self, tag):
        self.__tag_words = self.__get_tag_word(tag)
        self.update()

    def get_current_word(self):
        return self.__current_word

    def update(self):
        if len(self.__tag_words) != 0:
            self.__current_word = random.choice(self.__tag_words)
        else:
            self.__current_word = None

    # Private variables and constants.
    __words = None
    __tag_words = None
    __current_word = None

    # Private methods.
    def __get_tag_word(self, tag):
        r_words = []
        if len(tag) == 0:
            r_words = self.__words.copy()
        else:
            for i in self.__words:
                if tag in i.tags:
                    r_words.append(i)
        return r_words
