import os
from operator import attrgetter

from low_level import path_list


class UserWordListController:
    # Public methods.
    def get_list_len(self):
        return len(self.__lists)

    def get_sorted_name_list(self):
        names = []
        for i in self.__lists:
            names.append(i[0])
        return sorted(names)

    def get_list(self, name):
        ids = self.__get_list_ids(name)
        if not ids:
            return []
        words = []
        for i in self.__common_word_list.get_list():
            if i.w_id in ids:
                words.append(i)
        return words

    def get_sorted_list(self, name):
        return sorted(self.get_list(name), key=attrgetter("jp"))

    def get_sorted_free_list(self, name, tag):
        free_words = []

        added_words = self.get_sorted_list(name)
        if len(tag) == 0:
            for i in self.__common_word_list.get_list():
                if i not in added_words:
                    free_words.append(i)
        else:
            for i in self.__common_word_list.get_list():
                if (i not in added_words) and (tag in i.tags):
                    free_words.append(i)
        return sorted(free_words, key=attrgetter("jp"))

    def replace_name(self, old, new):
        lst = self.__get_list(old)
        lst[0] = new

    def add_word(self, name, word):
        ids = self.__get_list_ids(name)
        ids.append(word.w_id)

    def remove_word(self, name, word):
        ids = self.__get_list_ids(name)
        ids.remove(word.w_id)

    def add_empty_list(self):
        self.__lists.append(["", []])

    def remove_list(self, name):
        lst = self.__get_list(name)
        if lst is None:
            return
        self.__lists.remove(lst)

    def is_list_exist(self, name):
        return self.__get_list_ids(name) is not None

    def is_empty_list_exist(self):
        return ["", []] in self.__lists

    def read(self):
        self.__lists = []
        for f_name in os.listdir(self.__path_list.lists_path):
            with open(os.path.join(self.__path_list.lists_path, f_name), 'r', encoding='utf8') as f:
                ids = []
                while True:
                    line = f.readline()
                    if not line:
                        break
                    str_id = line.replace("\n", "")
                    if len(str_id) != 0:
                        ids.append(int(str_id))
                f.close()
                l_name = f_name.replace(".txt", "")
                self.__lists.append([l_name, ids])

    def save(self):
        # Remove old files.
        filelist = [f for f in os.listdir(self.__path_list.lists_path)]
        for f in filelist:
            os.remove(os.path.join(self.__path_list.lists_path, f))

        # Save new files.
        for i in self.__lists:
            f = open(self.__path_list.lists_path + "/" + i[0] + ".txt", "w", encoding='utf8')
            for w_id in i[1]:
                f.writelines(str(w_id) + "\n")
            f.close()

    # Private variables and constants.
    __path_list = None

    __lists = None
    __common_word_list = None

    # Private methods.
    def __init__(self, common_word_list_controller):
        self.__path_list = path_list.path_list()
        self.__common_word_list = common_word_list_controller
        self.read()

    def __get_list(self, name):
        for i in self.__lists:
            if i[0] != name:
                continue
            return i
        return None

    def __get_list_ids(self, name):
        l = self.__get_list(name)
        return l[1] if l is not None else None
