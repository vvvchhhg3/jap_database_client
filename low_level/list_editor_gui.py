from PyQt5.QtWidgets import QHBoxLayout
from low_level.funcs_gui import get_enabled_button, get_disabled_button
from low_level.list_viewer_gui import ListViewerGui


class ListEditorGui(ListViewerGui):
    pb_add = None
    pb_remove = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.pb_add = get_enabled_button("Add")
        self.pb_remove = get_disabled_button("Remove")
        # Init GUI.
        l_buttons = QHBoxLayout()
        self.l_main.addLayout(l_buttons)
        l_buttons.addWidget(self.pb_add)
        l_buttons.addWidget(self.pb_remove)
