from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QComboBox, QSizePolicy, QPushButton, QLineEdit, QCheckBox, QListWidget, QScroller, \
    QAbstractItemView, QTableWidgetItem, QTextEdit

def_font_size = 25


def get_non_selectable_table_item(text):
    i = QTableWidgetItem(text)
    font = i.font()
    font.setPointSize(def_font_size)
    i.setFont(font)
    i.setFlags(i.flags() ^ (Qt.ItemIsEditable | Qt.ItemIsSelectable))
    return i


def get_non_selectable_textedit(text):
    te = QTextEdit()
    te.setText(text)
    font = te.font()
    font.setPointSize(def_font_size)
    te.setFont(font)
    te.setTextInteractionFlags(Qt.TextInteractionFlag.NoTextInteraction)
    return te


def get_size_fill_horizontal_policy():
    size_policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
    size_policy.setHorizontalStretch(1)
    return size_policy


def get_label(text):
    label = QLabel(text)
    label.setWordWrap(True)
    return label


def get_big_label(text):
    label = QLabel(text)
    font = label.font()
    font.setPointSize(def_font_size)
    label.setWordWrap(True)
    label.setFont(font)
    return label


def get_big_center_label(text):
    label = get_big_label(text)
    label.setAlignment(Qt.AlignHCenter)
    return label


def get_center_label(text):
    label = QLabel(text)
    label.setAlignment(Qt.AlignHCenter)
    return label


def get_left_label(text):
    label = get_label(text)
    label.setAlignment(Qt.AlignLeft)
    return label


def get_line_edit():
    le = QLineEdit()
    font = le.font()
    font.setPointSize(def_font_size)
    le.setFont(font)
    le.setSizePolicy(get_size_fill_horizontal_policy())
    le.setFixedHeight(35)
    return le


def get_disabled_line_edit():
    le = get_line_edit()
    le.setEnabled(False)
    return le


def get_combobox():
    cb = QComboBox()
    font = cb.font()
    font.setPointSize(def_font_size)
    cb.setFont(font)
    cb.setFixedHeight(35)
    QScroller.grabGesture(
        cb.view().viewport(), QScroller.LeftMouseButtonGesture
    )
    cb.view().setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
    cb.setSizePolicy(get_size_fill_horizontal_policy())
    return cb


def get_disabled_combobox():
    cb = get_combobox()
    cb.setEnabled(False)
    return cb


def get_enabled_button(text, min_height=0):
    pb = QPushButton(text)
    if min_height >= 0:
        if min_height != 0:
            pb.setMinimumHeight(min_height)
        else:
            pb.setFixedHeight(35)
    else:
        pb.setSizePolicy(get_size_fill_horizontal_policy())
    return pb


def get_disabled_button(text, min_height=0):
    pb = get_enabled_button(text, min_height)
    pb.setEnabled(False)
    return pb


def get_enabled_square_button(text, size):
    pb = QPushButton()
    pb.setText(text)
    pb.setFixedSize(size, size)
    return pb


def get_disabled_square_button(text, size):
    pb = get_enabled_square_button(text, size)
    pb.setEnabled(False)
    return pb


def get_disabled_checkbox(text):
    cb = QCheckBox(text)
    cb.setEnabled(False)
    return cb


def get_list_widget(font_size):
    lw = QListWidget()
    font = lw.font()
    font.setPointSize(font_size)
    lw.setFont(font)
    QScroller.grabGesture(
        lw.viewport(), QScroller.LeftMouseButtonGesture
    )
    lw.setWordWrap(True)
    lw.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
    lw.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    lw.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

    lw.setStyleSheet("QListWidget::item { border-bottom: 1px solid black; color:#000000 } "
                     "QListView::item:selected { background-color: gray }")
    return lw
