import os
from operator import attrgetter

from low_level import path_list
from low_level.item_types import word_item


class CommonWordListController:
    # Public methods.
    def get_list(self):
        return self.__words.copy()

    def get_sorted_list(self):
        return sorted(self.__words, key=attrgetter("jp"))

    def add_empty_word(self):
        self.__word_last_id = self.__word_last_id + 1
        self.__words.append(word_item(self.__word_last_id, "", "", [], []))

    def remove_word(self, word):
        self.__words.remove(word)

    def replace_word(self, old, new):
        self.__words[self.__words.index(old)] = new

    def is_empty_word_contained(self):
        for i in self.__words:
            if (len(i.jp) != 0) or (len(i.kanji) != 0) or (len(i.tags) != 0) or (len(i.translate) != 0):
                continue
            return True
        return False

    def is_word_contained(self, jp, kanji):
        for i in self.__words:
            if i.jp != jp:
                continue
            if i.kanji != kanji:
                continue
            return True
        return False

    def read(self):
        f = open(self.__path_list.words_path, "r", encoding='utf8')
        self.__words = []
        file_header = True
        while True:
            line = f.readline()
            if not line:
                break
            if file_header:
                file_header = False
                continue
            line_data = line.split("\t")
            self.__words.append(word_item(int(line_data[0]), line_data[1], line_data[2], line_data[3].split(","),
                                          line_data[4].replace("\n", "").split(",")))
        f.close()

    def save(self):
        f = open(self.__path_list.words_path, "w", encoding='utf8')
        f.writelines("id\tjp\tkanji\ttags\ttranslate\n")
        for w in self.__words:
            if len(w.jp) == 0:
                continue
            tags = self.__get_tags_str(w)
            tr = self.__get_translate_str(w)
            f.writelines(str(w.w_id) + "\t" + w.jp + "\t" + w.kanji + "\t" + tags + "\t" + tr + "\n")
        f.close()

    # Private variables and constants.
    __path_list = None

    __words = None
    __word_last_id = None

    # Private methods.
    def __init__(self):
        self.__path_list = path_list.path_list()
        if os.path.isfile(self.__path_list.words_path):
            self.read()
        else:
            self.__words = []
            self.save()
        self.__count_word_last_id()

    def __count_word_last_id(self):
        self.__word_last_id = 0
        for i in self.__words:
            if self.__word_last_id < i.w_id:
                self.__word_last_id = i.w_id

    @staticmethod
    def __get_tags_str(word):
        tags = ""
        for i in word.tags:
            if len(tags) != 0:
                tags += ","
            tags += i
        return tags

    @staticmethod
    def __get_translate_str(word):
        translate = ""
        for i in word.translate:
            if len(translate) != 0:
                translate += ","
            translate += i
        return translate
