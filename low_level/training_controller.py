import enum
import os

from low_level import path_list
from low_level.item_types import card_training_item


class TrainingController:
    class Type(enum.IntEnum):
        JpToKanji = 0
        KanjiToJp = 1
        JpToTranslate = 2
        TranslateToJp = 3
        KanjiToTranslate = 4
        TranslateToKanji = 5

    def get_word_rating(self, w_id, r_type):
        for i in self.__training:
            if w_id == i.w_id:
                match r_type:
                    case r_type.JpToKanji:
                        return i.jp_to_kanji
                    case r_type.KanjiToJp:
                        return i.kanji_to_jp
                    case r_type.JpToTranslate:
                        return i.jp_to_translate
                    case r_type.TranslateToJp:
                        return i.translate_to_jp
                    case r_type.KanjiToTranslate:
                        return i.kanji_to_translate
                    case r_type.TranslateToKanji:
                        return i.translate_to_kanji
        return 0

    def inc_word_rating(self, w_id, r_type):
        for i in range(len(self.__training)):
            if w_id == self.__training[i].w_id:
                self.__inc_word_rating(i, r_type)
                return
        self.__training.append(card_training_item(w_id, 0, 0, 0, 0, 0, 0))
        self.__inc_word_rating(len(self.__training) - 1, r_type)

    def dec_word_rating(self, w_id, r_type):
        for i in range(len(self.__training)):
            if w_id == self.__training[i].w_id:
                self.__dec_word_rating(i, r_type)
                return
        self.__training.append(card_training_item(w_id, 0, 0, 0, 0, 0, 0))

    def read(self):
        f = open(self.__path_list.training_path, "r", encoding='utf8')
        self.__training = []
        file_header = True
        while True:
            line = f.readline()
            if not line:
                break
            if file_header:
                file_header = False
                continue
            line_data = line.split("\t")
            self.__training.append(card_training_item(int(line_data[0]),
                                                      int(line_data[1]), int(line_data[2]),
                                                      int(line_data[3]), int(line_data[4]),
                                                      int(line_data[5]), int(line_data[6].replace("\n", ""))))
        f.close()

    def save(self):
        f = open(self.__path_list.training_path, "w", encoding='utf8')
        f.writelines("id\tjp_to_kanji\tkanji_to_jp\tjp_to_translate\ttranslate_to_jp\t"
                     "kanji_to_translate\ttranslate_to_kanji\n")
        for w in self.__training:
            f.writelines(str(w.w_id) + "\t" +
                         str(w.jp_to_kanji) + "\t" + str(w.kanji_to_jp) + "\t" +
                         str(w.jp_to_translate) + "\t" + str(w.translate_to_jp) + "\t" +
                         str(w.kanji_to_translate) + "\t" + str(w.translate_to_kanji) + "\n")
        f.close()

    # Private variables and constants.
    __path_list = None
    __common_word_list = None

    __training = None

    # Private methods.
    def __init__(self, common_word_list_controller):
        self.__path_list = path_list.path_list()
        self.__common_word_list = common_word_list_controller
        if os.path.isfile(self.__path_list.training_path):
            self.read()
        else:
            self.__training = []
            self.save()

    def __inc_word_rating(self, i, r_type):
        old = self.__training[i]
        match r_type:
            case r_type.JpToKanji:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji + 1, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.KanjiToJp:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp + 1,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.JpToTranslate:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate + 1, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.TranslateToJp:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp + 1,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.KanjiToTranslate:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate + 1, old.translate_to_kanji)
            case r_type.TranslateToKanji:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji + 1)

    def __dec_word_rating(self, i, r_type):
        old = self.__training[i]
        match r_type:
            case r_type.JpToKanji:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji - 1 if old.jp_to_kanji > 1 else 0,
                                                        old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.KanjiToJp:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji,
                                                        old.kanji_to_jp - 1 if old.kanji_to_jp > 1 else 0,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.JpToTranslate:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate - 1 if old.jp_to_translate > 1 else 0,
                                                        old.translate_to_jp,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.TranslateToJp:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate,
                                                        old.translate_to_jp - 1 if old.translate_to_jp > 1 else 0,
                                                        old.kanji_to_translate, old.translate_to_kanji)
            case r_type.KanjiToTranslate:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate - 1 if old.kanji_to_translate > 1 else 0,
                                                        old.translate_to_kanji)
            case r_type.TranslateToKanji:
                self.__training[i] = card_training_item(old.w_id,
                                                        old.jp_to_kanji, old.kanji_to_jp,
                                                        old.jp_to_translate, old.translate_to_jp,
                                                        old.kanji_to_translate,
                                                        old.translate_to_kanji - 1 if old.translate_to_kanji > 1 else 0)
