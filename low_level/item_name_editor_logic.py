from PyQt5.QtCore import QObject


class ItemNameEditorLogic(QObject):
    # Public methods.
    def set(self, name):
        self.__default_name = name
        self.__set_text_safely(self.__default_name)
        self.__update_gui_access()

    def reset(self):
        self.__default_name = None
        self.__set_text_safely("")
        self.__update_gui_access()

    # Protected virtual functions.
    def _replace_name(self, old, new):
        pass  # Replace in object.

    # Private variables and constants.
    __gui = None
    __default_name = None
    __is_name_exist = None

    # Private methods.
    def __init__(self, gui, is_name_exist):
        super().__init__()
        # Init objects.
        self.__gui = gui
        self.__is_name_exist = is_name_exist
        # Init connects.
        self.__gui.le_line.textChanged.connect(self.__text_changed)
        self.__gui.pb_clear.clicked.connect(self.__clear_clicked)
        self.__gui.pb_save.clicked.connect(self.__save_clicked)
        self.__gui.pb_reset.clicked.connect(self.__reset_clicked)

    def __set_text_safely(self, text):
        self.__gui.le_line.textChanged.disconnect(self.__text_changed)
        self.__gui.le_line.setText(text)
        self.__gui.le_line.textChanged.connect(self.__text_changed)

    def __update_gui_access(self):
        if self.__default_name is None:
            self.__gui.pb_clear.setEnabled(False)
            self.__gui.pb_save.setEnabled(False)
            self.__gui.pb_reset.setEnabled(False)
            self.__gui.le_line.setEnabled(False)
            self.__set_text_safely("")
            return

        self.__gui.le_line.setEnabled(True)
        t = self.__gui.le_line.text()
        self.__gui.pb_save.setEnabled((len(t) != 0) and
                                      (t != self.__default_name) and
                                      (not self.__is_name_exist(self.__gui.le_line.text())))
        self.__gui.pb_reset.setEnabled(t != self.__default_name)
        self.__gui.pb_clear.setEnabled(len(t) != 0)

    # Private slots.
    def __text_changed(self):
        self.__update_gui_access()

    def __clear_clicked(self):
        self.__set_text_safely("")
        self.__update_gui_access()

    def __save_clicked(self):
        new = self.__gui.le_line.text()
        old = self.__default_name
        self.__default_name = new
        self.__update_gui_access()
        self._replace_name(old, new)

    def __reset_clicked(self):
        self.__set_text_safely(self.__default_name)
        self.__update_gui_access()
