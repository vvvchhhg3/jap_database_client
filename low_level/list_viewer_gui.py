from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout
from low_level.funcs_gui import get_list_widget, get_line_edit, get_label


class ListViewerGui(QWidget):
    lw_items = None
    le_find = None
    l_main = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.lw_items = get_list_widget(20)
        self.le_find = get_line_edit()
        # Init GUI.
        self.l_main = QVBoxLayout(self)
        self.l_main.setContentsMargins(0, 0, 0, 0)
        self.l_main.addWidget(self.lw_items)
        l_find = QHBoxLayout()
        self.l_main.addLayout(l_find)
        l_find.addWidget(get_label("Find: "))
        l_find.addWidget(self.le_find)
