from PyQt5.QtCore import QObject


class ListViewerLogic(QObject):
    # Public methods.
    def set(self, items):
        self.__items = items
        self.clear_find_line()
        self.__reshow()
        self.set_row(0)

    def select(self, item):
        if item not in self.__shown_items:
            return
        self._gui.lw_items.setCurrentRow(self.__shown_items.index(item))

    def unselect(self):
        self.__items = None
        self.__shown_items = []
        self._gui.lw_items.currentRowChanged.disconnect(self.__row_changed)
        self._gui.lw_items.clear()
        self._gui.lw_items.currentRowChanged.connect(self.__row_changed)
        self.clear_find_line()

    def clear_find_line(self):
        self._gui.le_find.textChanged.disconnect(self.__text_changed)
        self._gui.le_find.clear()
        self._gui.le_find.textChanged.connect(self.__text_changed)

    def get_current_row(self):
        return self._gui.lw_items.currentRow()

    def get_current_item(self):
        return self.__shown_items[self.get_current_row()]

    def get_shown_items_num(self):
        return len(self.__shown_items)

    def set_row(self, row):
        if row < len(self.__shown_items):
            self._gui.lw_items.setCurrentRow(row)
        else:
            self._item_unselected()

    # Protected variables and constants.
    _gui = None

    # Protected virtual functions.
    def _item_selected(self, item):
        pass  # Send qt signal.

    def _item_unselected(self):
        pass  # Send qt signal.

    def _get_string(self, item):
        pass  # Return string from item.

    def _is_need_show(self, item, mask):
        pass  # Return boolean.

    # Private variables and constants.
    __items = None
    __shown_items = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self._gui = gui
        self.__shown_items = []
        # Init connects.
        self._gui.lw_items.currentRowChanged.connect(self.__row_changed)
        self._gui.le_find.textChanged.connect(self.__text_changed)

    def __add_item(self, item):
        self.__shown_items.append(item)
        self._gui.lw_items.addItem(self._get_string(item))

    def __show_with_contains_part(self, mask):
        for i in self.__items:
            if self._is_need_show(i, mask):
                self.__add_item(i)

    def __show_all(self):
        for i in self.__items:
            self.__add_item(i)

    def __reshow(self):
        self._gui.lw_items.currentRowChanged.disconnect(self.__row_changed)
        self._gui.lw_items.clear()
        self.__shown_items.clear()
        text = self._gui.le_find.text()
        if len(text) != 0:
            self.__show_with_contains_part(text)
        else:
            self.__show_all()
        self._gui.lw_items.currentRowChanged.connect(self.__row_changed)

    # Private slots.
    def __row_changed(self, row):
        self._item_selected(self.__shown_items[row])

    def __text_changed(self):
        self.__reshow()
        self.set_row(0)
