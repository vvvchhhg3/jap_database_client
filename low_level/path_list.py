from collections import namedtuple
from pathlib import Path


def path_list():
    paths = namedtuple("paths", "root_path,words_path,tags_path,lists_path,grammars_path,training_path")
    home = Path.home()
    return paths(root_path=str(home.joinpath("jp_db_client")),
                 words_path=str(home.joinpath("jp_db_client/words.csv")),
                 tags_path=str(home.joinpath("jp_db_client/tags.txt")),
                 lists_path=str(home.joinpath("jp_db_client/lists")),
                 grammars_path=str(home.joinpath("jp_db_client/grammars.xml")),
                 training_path=str(home.joinpath("jp_db_client/training.csv")))
