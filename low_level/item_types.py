from collections import namedtuple


def grammar_example_item(ex_id, jp, furigana):
    item = namedtuple("grammar_example_item", "ex_id,jp,furigana")
    return item(ex_id=ex_id, jp=jp, furigana=furigana)


def grammar_item(gr_id, jp, translate, examples):
    item = namedtuple("grammar_item", "gr_id,jp,translate,examples")
    return item(gr_id=gr_id, jp=jp, translate=translate, examples=examples)


def word_item(w_id, jp, kanji, tags, translate):
    item = namedtuple("word_item", "w_id,jp,kanji,tags,translate")
    return item(w_id=w_id, jp=jp, kanji=kanji, tags=tags, translate=translate)


def card_training_item(w_id,
                       jp_to_kanji, kanji_to_jp,
                       jp_to_translate, translate_to_jp,
                       kanji_to_translate, translate_to_kanji):
    item = namedtuple("card_training_item", "w_id,"
                                            "jp_to_kanji,kanji_to_jp,"
                                            "jp_to_translate,translate_to_jp,"
                                            "kanji_to_translate,translate_to_kanji")
    return item(w_id=w_id,
                jp_to_kanji=jp_to_kanji, kanji_to_jp=kanji_to_jp,
                jp_to_translate=jp_to_translate, translate_to_jp=translate_to_jp,
                kanji_to_translate=kanji_to_translate, translate_to_kanji=translate_to_kanji)
