from low_level.list_viewer_logic import ListViewerLogic


class ListEditorLogic(ListViewerLogic):
    # Public methods.
    def set(self, items):
        super().set(items)
        self.update_gui_access()

    def update_gui_access(self):
        self.__gui.pb_add.setEnabled(not self._is_empty_object_contains())
        self.__gui.pb_remove.setEnabled(super().get_shown_items_num() != 0)

    # Protected virtual functions.
    def _save_items(self):
        pass  # Send qt signal.

    def _add_empty_item(self):
        pass  # Call "Add empty item" in data controller / insert item in list.

    def _remove_item(self, item):
        pass  # Call "Remove item" in data controller / remove item in list.

    def _get_items(self):
        pass  # Return item list from controller / return list.

    def _is_empty_object_contains(self):
        pass  # Return boolean.

    # Private variables and constants.
    __gui = None
    __lv = None

    # Private methods.
    def __init__(self, gui):
        super().__init__(gui)
        # Init objects.
        self.__gui = gui
        # Init GUI.
        self.__gui.pb_add.clicked.connect(self.__add_clicked)
        self.__gui.pb_remove.clicked.connect(self.__remove_clicked)

    # Private slots.
    def __add_clicked(self):
        self._add_empty_item()
        super().set(self._get_items())
        self.set_row(0)
        self.update_gui_access()

    def __remove_clicked(self):
        item = self.get_current_item()
        row = self.get_current_row()
        self._remove_item(item)
        self._save_items()
        items = self._get_items()
        super().set(items)
        if len(items) != 0:
            if row > 0:
                row = row - 1
            self.set_row(row)
        self.update_gui_access()
