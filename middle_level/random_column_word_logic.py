from PyQt5.QtCore import QObject

from low_level.random_word import RandomWord


class RandomColumnWordLogic(QObject):
    # Public methods.
    def set(self, words, tags):
        self.__words = words
        self.__tags = tags
        if len(tags) == 0:
            self.__gui.cb_tag.setEnabled(False)
        else:
            self.__update_tags()
            self.__gui.cb_tag.setEnabled(True)
            self.__random_word.set_words(words)
            self.__random_word.set_tag("")
        self.__set_text()
        self.__update_gui_access()

    def update(self):
        self.__random_word.update()
        self.__set_text()

    # Private variables and constants.
    __gui = None
    __words = None
    __tags = None
    __random_word = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        self.__gui = gui
        self.__random_word = RandomWord()
        self.__gui.cb_jp.setChecked(True)
        self.__init_connects()

    def __init_connects(self):
        self.__gui.cb_jp.stateChanged.connect(self.__checkbox_state_changed)
        self.__gui.cb_kanji.stateChanged.connect(self.__checkbox_state_changed)
        self.__gui.cb_translate.stateChanged.connect(self.__checkbox_state_changed)
        self.__gui.cb_tags.stateChanged.connect(self.__checkbox_state_changed)
        self.__gui.cb_tag.currentIndexChanged.connect(self.__row_changed)

    def __get_string(self, word):
        s = ""
        if word is None:
            return s
        if self.__gui.cb_jp.isChecked():
            s += word.jp + "\n\n"
        if self.__gui.cb_kanji.isChecked() and (len(word.kanji) != 0):
            s += word.kanji + "\n\n"
        if self.__gui.cb_translate.isChecked():
            for i in word.translate:
                s += i + "\n"
            s += "\n"
        if self.__gui.cb_tags.isChecked():
            for i in word.tags:
                s += i + "\n"
            s += "\n"
        return s

    def __update_gui_access(self):
        self.__set_enabled(self.__random_word.get_current_word() is not None)

    def __update_tags(self):
        cur_text = self.__gui.cb_tag.currentText()
        self.__gui.cb_tag.currentIndexChanged.disconnect(self.__row_changed)
        self.__gui.cb_tag.clear()
        self.__gui.cb_tag.addItems([""] + self.__tags)
        self.__gui.cb_tag.setCurrentText(cur_text)
        self.__gui.cb_tag.currentIndexChanged.connect(self.__row_changed)

    def __set_enabled(self, enabled):
        self.__gui.cb_jp.setEnabled(enabled)
        self.__gui.cb_kanji.setEnabled(enabled)
        self.__gui.cb_translate.setEnabled(enabled)
        self.__gui.cb_tags.setEnabled(enabled)

    def __set_text(self):
        self.__gui.l_description.setText(self.__get_string(self.__random_word.get_current_word()))

    # Private slots.
    def __checkbox_state_changed(self):
        self.__set_text()

    def __row_changed(self):
        self.__random_word.set_tag(self.__gui.cb_tag.currentText())
        if self.__random_word.get_current_word() is not None:
            self.__set_text()
        else:
            self.__gui.l_button.clear()
        self.__update_gui_access()
