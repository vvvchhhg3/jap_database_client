from PyQt5.QtCore import pyqtSignal

from low_level.data_controller import DataController
from low_level.list_editor_logic import ListEditorLogic
from middle_level.list_user_word_list_name_viewer_logic import ListUserWordListNameViewerLogic


class ListUserWordListNameEditorLogic(ListUserWordListNameViewerLogic, ListEditorLogic):
    # Public signals.
    saved = pyqtSignal()

    # Protected virtual functions.
    def _save_items(self):
        DataController().user_word_list.save()
        self.saved.emit()

    def _add_empty_item(self):
        DataController().user_word_list.add_empty_list()

    def _remove_item(self, item):
        DataController().user_word_list.remove_list(item)

    def _get_items(self):
        return DataController().user_word_list.get_sorted_name_list()

    def _is_empty_object_contains(self):
        return DataController().user_word_list.is_empty_list_exist()
