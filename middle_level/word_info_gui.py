from PyQt5.QtWidgets import QWidget, QVBoxLayout, QFrame
from low_level.funcs_gui import get_big_label, get_label, get_center_label


class WordInfoGui(QWidget):
    l_name_jp = None
    l_name_kanji = None
    l_name_translate = None
    l_name_tags = None

    l_jp = None
    l_kanji = None
    l_translate = None
    l_tags = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.l_jp = get_big_label("")
        self.l_kanji = get_big_label("")
        self.l_translate = get_label("")
        self.l_tags = get_label("")

        self.l_name_jp = get_center_label("Jp")
        self.l_name_kanji = get_center_label("Kanji")
        self.l_name_translate = get_center_label("Translate")
        self.l_name_tags = get_center_label("Tags")

        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        fr = QFrame()
        fr.setFrameShape(QFrame.StyledPanel)
        l_main.addWidget(fr)

        l_items = QVBoxLayout()
        fr.setLayout(l_items)

        l_items.addWidget(self.l_name_jp)
        l_items.addWidget(self.l_jp)
        l_items.addWidget(self.l_name_kanji)
        l_items.addWidget(self.l_kanji)
        l_items.addWidget(self.l_name_translate)
        l_items.addWidget(self.l_translate)
        l_items.addWidget(self.l_name_tags)
        l_items.addWidget(self.l_tags)
        l_items.addStretch(1)
