from PyQt5.QtWidgets import QWidget, QVBoxLayout

from low_level.funcs_gui import get_disabled_combobox, get_disabled_checkbox, get_label, get_center_label


class RandomColumnWordGui(QWidget):
    cb_tag = None
    cb_jp = None
    cb_kanji = None
    cb_translate = None
    cb_tags = None
    l_description = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.cb_tag = get_disabled_combobox()
        self.cb_jp = get_disabled_checkbox("Jp")
        self.cb_kanji = get_disabled_checkbox("Kanji")
        self.cb_translate = get_disabled_checkbox("Translate")
        self.cb_tags = get_disabled_checkbox("Tags")
        self.l_description = get_label("")
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        l_main.addWidget(get_center_label("Tag"))
        l_main.addWidget(self.cb_tag)
        l_main.addWidget(get_center_label("Show"))
        l_main.addWidget(self.cb_jp)
        l_main.addWidget(self.cb_kanji)
        l_main.addWidget(self.cb_translate)
        l_main.addWidget(self.cb_tags)
        l_main.addWidget(self.l_description)
        l_main.addStretch(1)
