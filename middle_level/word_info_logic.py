from PyQt5.QtCore import QObject


class WordInfoLogic(QObject):
    # Public methods.
    def set(self, word):
        self.__gui.l_jp.setText(word.jp)
        self.__gui.l_kanji.setText(word.kanji)
        tr = ""
        for i in word.translate:
            tr += i + "\n"
        self.__gui.l_translate.setText(tr)
        tags = ""
        for i in word.tags:
            tags += i + "\n"
        self.__gui.l_tags.setText(tags)
        self.__set_show()

    def reset(self):
        self.__set_hide()

    # Private variables and constants.
    __gui = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        self.__gui = gui
        self.__set_hide()

    def __set_show(self):
        self.__gui.l_name_jp.show()
        self.__gui.l_name_kanji.show()
        self.__gui.l_name_translate.show()
        self.__gui.l_name_tags.show()
        self.__gui.l_jp.show()
        self.__gui.l_kanji.show()
        self.__gui.l_translate.show()
        self.__gui.l_tags.show()

    def __set_hide(self):
        self.__gui.l_name_jp.hide()
        self.__gui.l_name_kanji.hide()
        self.__gui.l_name_translate.hide()
        self.__gui.l_name_tags.hide()
        self.__gui.l_jp.hide()
        self.__gui.l_kanji.hide()
        self.__gui.l_translate.hide()
        self.__gui.l_tags.hide()
