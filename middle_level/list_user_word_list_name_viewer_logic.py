from PyQt5.QtCore import pyqtSignal

from low_level.list_viewer_logic import ListViewerLogic


class ListUserWordListNameViewerLogic(ListViewerLogic):
    # Public signals.
    selected = pyqtSignal(str)
    unselected = pyqtSignal()

    # Protected virtual functions.
    def _item_selected(self, item):
        self.selected.emit(item)

    def _item_unselected(self):
        self.unselected.emit()

    def _get_string(self, item):
        return item

    def _is_need_show(self, item, mask):
        return mask in item
