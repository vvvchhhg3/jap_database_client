from PyQt5.QtCore import pyqtSignal

from low_level.list_viewer_logic import ListViewerLogic


class GrammarListViewerLogic(ListViewerLogic):
    # Public signals.
    selected = pyqtSignal(tuple)
    unselected = pyqtSignal()

    # Protected virtual functions.
    def _item_selected(self, item):
        self.selected.emit(item)

    def _item_unselected(self):
        self.unselected.emit()

    def _get_string(self, item):
        return item.jp + "\n\n" + item.translate

    def _is_need_show(self, item, mask):
        if (mask in item.jp) or (mask in item.translate):
            return True
        else:
            return False
