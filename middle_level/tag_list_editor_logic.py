from PyQt5.QtCore import pyqtSignal

from low_level.data_controller import DataController
from low_level.list_editor_logic import ListEditorLogic
from middle_level.tag_list_viewer_logic import TagListViewerLogic


class TagListEditorLogic(TagListViewerLogic, ListEditorLogic):
    # Public signals.
    saved = pyqtSignal()

    # Protected virtual functions.
    def _save_items(self):
        DataController().tag_list.save()
        self.saved.emit()

    def _add_empty_item(self):
        DataController().tag_list.add_empty_tag()

    def _remove_item(self, item):
        DataController().tag_list.remove_tag(item)

    def _get_items(self):
        return DataController().tag_list.get_sorted_list()

    def _is_empty_object_contains(self):
        return DataController().tag_list.is_empty_tag_exist()
