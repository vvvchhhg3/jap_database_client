from PyQt5.QtWidgets import QWidget, QVBoxLayout, QFrame

from low_level.funcs_gui import get_big_label, get_list_widget, get_center_label


class GrammarInfoGui(QWidget):
    l_grammar = None
    lw_examples = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.l_grammar = get_big_label("")
        self.lw_examples = get_list_widget(20)
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        fr = QFrame()
        fr.setFrameShape(QFrame.StyledPanel)
        l_main.addWidget(fr)

        l_items = QVBoxLayout()
        fr.setLayout(l_items)

        l_items.addWidget(get_center_label("Grammar"))
        l_items.addWidget(self.l_grammar)
        l_items.addWidget(get_center_label("Examples"))
        l_items.addWidget(self.lw_examples)
