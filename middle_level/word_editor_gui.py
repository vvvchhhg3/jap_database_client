from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_disabled_line_edit, get_disabled_combobox, get_disabled_button, get_center_label, \
    get_label


class WordEditorGui(QWidget):
    num_max_tag = 5
    num_max_translate = 5

    le_jp = None
    le_kanji = None
    cb_tags = [None] * num_max_tag
    le_translates = [None] * num_max_translate
    pb_clear = None
    pb_save = None
    pb_reset = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.le_jp = get_disabled_line_edit()
        self.le_kanji = get_disabled_line_edit()
        for i in range(self.num_max_tag):
            self.cb_tags[i] = get_disabled_combobox()
        for i in range(self.num_max_translate):
            self.le_translates[i] = get_disabled_line_edit()
        self.pb_clear = get_disabled_button("Clear")
        self.pb_save = get_disabled_button("Save")
        self.pb_reset = get_disabled_button("Reset")
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        l_main.addWidget(get_center_label("Jp"))
        l_main.addWidget(self.le_jp)
        l_main.addWidget(get_center_label("Kanji"))
        l_main.addWidget(self.le_kanji)
        l_main.addWidget(get_center_label("Tags"))
        for i in range(self.num_max_tag):
            qh_layout = QHBoxLayout()
            l_main.addLayout(qh_layout)
            qh_layout.addWidget(get_label(str(i + 1) + ": "))
            qh_layout.addWidget(self.cb_tags[i])
        l_main.addWidget(get_center_label("Translate"))
        for i in range(self.num_max_translate):
            qh_layout = QHBoxLayout()
            l_main.addLayout(qh_layout)
            qh_layout.addWidget(get_label(str(i + 1) + ": "))
            qh_layout.addWidget(self.le_translates[i])
        l_buttons = QHBoxLayout()
        l_main.addStretch(1)
        l_main.addLayout(l_buttons)
        l_buttons.addWidget(self.pb_clear)
        l_buttons.addWidget(self.pb_save)
        l_buttons.addWidget(self.pb_reset)

