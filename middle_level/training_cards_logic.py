from PyQt5.QtCore import QObject, pyqtSignal

from low_level.data_controller import DataController


class TrainingCardsLogic(QObject):
    # Public signals.
    ended = pyqtSignal()

    # Public methods.
    def set(self, tr_type, words):
        self.__tr_type = tr_type
        self.__words = words
        self.__cur_num = 0
        self.__cur_side = 0
        self.__inc_words = []
        self.__dec_words = []
        self.__gui.l_button.clear()
        self.__gui.pb_next.setEnabled(False)
        self.__gui.pb_no.setEnabled(False)
        self.__gui.pb_yes.setEnabled(False)
        if len(self.__words) != 0:
            self.__gui.pb_word.setEnabled(True)
            self.__gui.l_button.setText(self.__get_button_text())
        else:
            self.__gui.pb_word.setEnabled(False)

    # Private variables and constants.
    __gui = None
    __tr_type = None
    __words = None
    __cur_num = None
    __cur_side = None
    __inc_words = None
    __dec_words = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__gui = gui
        self.__words = []
        self.__cur_num = 0
        self.__cur_side = 0
        # Init connects.
        self.__gui.pb_word.clicked.connect(self.__word_clicked)
        self.__gui.pb_no.clicked.connect(self.__no_clicked)
        self.__gui.pb_yes.clicked.connect(self.__yes_clicked)
        self.__gui.pb_next.clicked.connect(self.__next_clicked)

    def __get_button_text(self):
        w_cur = self.__words[self.__cur_num]
        match self.__tr_type:
            case self.__tr_type.JpToKanji:
                return w_cur.jp if self.__cur_side == 0 else w_cur.kanji
            case self.__tr_type.KanjiToJp:
                return w_cur.kanji if self.__cur_side == 0 else w_cur.jp
            case self.__tr_type.JpToTranslate:
                return w_cur.jp if self.__cur_side == 0 else self.__get_translate_str(w_cur.translate)
            case self.__tr_type.TranslateToJp:
                return self.__get_translate_str(w_cur.translate) if self.__cur_side == 0 else w_cur.jp
            case self.__tr_type.KanjiToTranslate:
                return w_cur.kanji if self.__cur_side == 0 else self.__get_translate_str(w_cur.translate)
            case self.__tr_type.TranslateToKanji:
                return self.__get_translate_str(w_cur.translate) if self.__cur_side == 0 else w_cur.kanji

    @staticmethod
    def __get_translate_str(w_tr):
        translate = ""
        for i in w_tr:
            if len(translate) != 0:
                translate += "\n"
            translate += i
        return translate

    def __yes_no_common_handler(self):
        self.__gui.pb_no.setEnabled(False)
        self.__gui.pb_yes.setEnabled(False)
        self.__cur_side = 0
        self.__cur_num = self.__cur_num + 1
        if self.__cur_num != len(self.__words):
            self.__gui.pb_next.setEnabled(True)
        else:
            self.__gui.pb_next.setEnabled(False)
            self.__gui.l_button.clear()
            self.__change_rating()
            self.ended.emit()

    def __change_rating(self):
        for i in self.__dec_words:
            DataController().training.dec_word_rating(i, self.__tr_type)
        for i in self.__inc_words:
            DataController().training.inc_word_rating(i, self.__tr_type)
        DataController().training.save()

    # Private slots.
    def __word_clicked(self):
        self.__cur_side = 1 if self.__cur_side == 0 else 0
        self.__gui.l_button.setText(self.__get_button_text())
        self.__gui.pb_word.setEnabled(False)
        self.__gui.pb_no.setEnabled(True)
        self.__gui.pb_yes.setEnabled(True)

    def __no_clicked(self):
        self.__dec_words.append(self.__words[self.__cur_num].w_id)
        self.__yes_no_common_handler()

    def __yes_clicked(self):
        self.__inc_words.append(self.__words[self.__cur_num].w_id)
        self.__yes_no_common_handler()

    def __next_clicked(self):
        self.__gui.pb_next.setEnabled(False)
        self.__gui.l_button.clear()
        self.__gui.l_button.setText(self.__get_button_text())
        self.__gui.pb_word.setEnabled(True)
