from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_disabled_button, get_center_label


class TrainingCardsGui(QWidget):
    pb_word = None
    l_button = None
    pb_next = None
    pb_no = None
    pb_yes = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.pb_word = get_disabled_button("", -1)
        self.l_button = get_center_label("")
        self.l_button.setAlignment(Qt.AlignCenter)
        self.l_button.setTextInteractionFlags(Qt.NoTextInteraction)
        self.l_button.setWordWrap(True)
        self.l_button.setMouseTracking(False)
        font = self.l_button.font()
        font.setPointSize(40)
        self.l_button.setFont(font)
        self.pb_no = get_disabled_button("No", 80)
        self.pb_yes = get_disabled_button("Yes", 80)
        self.pb_next = get_disabled_button("Next", 80)

        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        l_main.addWidget(self.pb_word)

        l_answer_buttons = QHBoxLayout(self)
        l_main.addLayout(l_answer_buttons)
        l_answer_buttons.addWidget(self.pb_no)
        l_answer_buttons.addWidget(self.pb_yes)

        l_word_button = QHBoxLayout()
        self.pb_word.setLayout(l_word_button)
        l_word_button.addWidget(self.l_button)

        l_main.addWidget(self.pb_next)