from PyQt5.QtCore import pyqtSignal

from low_level.data_controller import DataController
from low_level.list_editor_logic import ListEditorLogic
from middle_level.word_list_viewer_logic import WordListViewerLogic


class CommonWordListEditorLogic(WordListViewerLogic, ListEditorLogic):
    # Public signals.
    saved = pyqtSignal()

    # Protected virtual functions.
    def _save_items(self):
        DataController().common_word_list.save()
        self.saved.emit()

    def _add_empty_item(self):
        DataController().common_word_list.add_empty_word()

    def _remove_item(self, item):
        DataController().common_word_list.remove_word(item)

    def _get_items(self):
        return DataController().common_word_list.get_sorted_list()

    def _is_empty_object_contains(self):
        return DataController().common_word_list.is_empty_word_contained()
