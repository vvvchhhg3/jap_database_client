from typing import Final
from PyQt5.QtCore import QObject


class KeyboardLogic(QObject):
    # Public methods.
    def focus_changed(self, old, new):
        if type(old).__name__ == "QLineEdit":
            old.cursorPositionChanged.disconnect(self.__cursor_pos_changed)

        if type(new).__name__ == "QLineEdit":
            self.__line_edit = new
            new.cursorPositionChanged.connect(self.__cursor_pos_changed)
            self.__set_keys_enabled(True)
            self.__update_gui_access()
        else:
            self.__line_edit = None
            self.__set_keys_enabled(False)

    # Private variables and constants.
    __gui = None
    __line_edit = None
    __KEYS_RU: Final = [["ё", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
                        ["й", "ц", "у", "к", "е", "н", "г", "ш", "щ", "з", "х", "ъ"],
                        ["ф", "ы", "в", "а", "п", "р", "о", "л", "д", "ж", "э"],
                        ["я", "ч", "с", "м", "и", "т", "ь", "б", "ю"]]
    __KEYS_RU_SHIFT: Final = [["Ё", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "", ""],
                              ["Й", "Ц", "У", "К", "Е", "Н", "Г", "Ш", "Щ", "З", "Х", "Ъ"],
                              ["Ф", "Ы", "В", "А", "П", "Р", "О", "Л", "Д", "Ж", "Э", ""],
                              ["Я", "Ч", "С", "М", "И", "Т", "Ь", "Б", "Ю", ""]]
    __KEYS_JP_HIRAGANA: Final = [["", "", "", "", "", "", "", "", "", "", ""],
                                 ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "", ""],
                                 ["a", "s", "d", "f", "g", "h", "j", "k", "l", "", ""],
                                 ["z", "x", "c", "v", "b", "n", "m", "", ""]]
    __KEYS_JP_KATAKANA: Final = [["", "", "", "", "", "", "", "", "", "ー", ""],
                                 ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "", ""],
                                 ["a", "s", "d", "f", "g", "h", "j", "k", "l", "", ""],
                                 ["z", "x", "c", "v", "b", "n", "m", "", ""]]
    __JP_TO_HIRAGANA: Final = [["gyuu", "ぎゅう"], ["gyou", "ぎょう"], ["jyuu", "じゅう"], ["jyou", "じょう"],
                               ["byuu", "びゅう"], ["byou", "びょう"], ["pyuu", "ぴゅう"], ["pyou", "ぴょう"],
                               ["kyuu", "きゅう"], ["kyou", "きょう"], ["shuu", "しゅう"], ["shou", "しょう"],
                               ["chuu", "ちゅう"], ["chou", "ちょう"], ["nyuu", "にゅう"], ["nyou", "にょう"],
                               ["hyuu", "ひゅう"], ["hyou", "ひょう"], ["myuu", "みゅう"], ["myou", "みょう"],
                               ["ryuu", "りゅう"], ["ryou", "りょう"],
                               ["gya", "ぎゃ"], ["gyu", "ぎゅ"], ["gyo", "ぎょ"], ["jya", "じゃ"], ["jyu", "じゅ"],
                               ["jyo", "じょ"], ["bya", "びゃ"], ["byu", "びゅ"], ["byo", "びょ"], ["pya", "ぴゃ"],
                               ["pyu", "ぴゅ"], ["pyo", "ぴょ"], ["chi", "ち"], ["tsu", "つ"], ["kya", "きゃ"],
                               ["kyu", "きゅ"], ["kyo", "きょ"], ["sha", "しゃ"], ["shu", "しゅ"], ["sho", "しょ"],
                               ["cha", "ちゃ"], ["chu", "ちゅ"], ["cho", "ちょ"], ["nya", "にゃ"], ["nyu", "にゅ"],
                               ["nyo", "にょ"], ["hya", "ひゃ"], ["hyu", "ひゅ"], ["hyo", "ひょ"], ["mya", "みゃ"],
                               ["myu", "みゅ"], ["myo", "みょ"], ["rya", "りゃ"], ["ryu", "りゅ"], ["ryo", "りょ"],
                               ["shi", "し"],
                               ["ka", "か"], ["ki", "き"], ["ku", "く"], ["ke", "け"], ["ko", "こ"], ["sa", "さ"],
                               ["su", "す"], ["se", "せ"], ["so", "そ"], ["ta", "た"], ["te", "て"], ["to", "と"],
                               ["na", "な"], ["ni", "に"], ["nu", "ぬ"], ["ne", "ね"], ["no", "の"], ["ha", "は"],
                               ["hi", "ひ"], ["fu", "ふ"], ["he", "へ"], ["ho", "ほ"], ["ma", "ま"], ["mi", "み"],
                               ["mu", "む"], ["me", "め"], ["mo", "も"], ["ya", "や"], ["yu", "ゆ"], ["yo", "よ"],
                               ["ra", "ら"], ["ri", "り"], ["ru", "る"], ["re", "れ"], ["ro", "ろ"], ["wa", "わ"],
                               ["wo", "を"], ["nn", "ん"], ["ga", "が"], ["gi", "ぎ"], ["gu", "ぐ"], ["ge", "げ"],
                               ["go", "ご"], ["za", "ざ"], ["ji", "じ"], ["zu", "ず"], ["ze", "ぜ"], ["zo", "ぞ"],
                               ["da", "だ"], ["di", "ぢ"], ["du", "づ"], ["de", "で"], ["do", "ど"], ["ba", "ば"],
                               ["bi", "び"], ["bu", "ぶ"], ["be", "べ"], ["bo", "ぼ"], ["pa", "ぱ"], ["pi", "ぴ"],
                               ["pu", "ぷ"], ["pe", "ぺ"], ["po", "ぽ"],
                               ["a", "あ"], ["i", "い"], ["u", "う"], ["e", "え"], ["o", "お"]]
    __JP_TO_KATAKANA: Final = [["dexyu", "デュ"],
                               ["kyuu", "キュウ"], ["kyou", "キョウ"], ["shuu", "シュウ"], ["shou", "ショウ"],
                               ["chuu", "チュウ"], ["chou", "チョウ"], ["nyuu", "ニュウ"], ["nyou", "ニョウ"],
                               ["hyuu", "ヒュウ"], ["hyou", "ヒョウ"], ["myuu", "ミュウ"], ["myou", "ミョウ"],
                               ["ryuu", "リュウ"], ["ryou", "リょウ"], ["gyuu", "ギュウ"], ["gyou", "ギョウ"],
                               ["jyuu", "ジュウ"], ["jyou", "ジョウ"], ["byuu", "ビュウ"], ["byou", "ビョウ"],
                               ["pyuu", "ピュウ"], ["pyou", "ピョウ"], ["texi", "ティ"], ["dexi", "ディ"],
                               ["toxu", "トゥ"],
                               ["kya", "キャ"], ["kyu", "キュ"], ["kyo", "キョ"], ["sha", "シャ"], ["shu", "シュ"],
                               ["sho", "ショ"], ["cha", "チャ"], ["chu", "チュ"], ["cho", "チョ"], ["nya", "ニャ"],
                               ["nyu", "ニュ"], ["nyo", "ニョ"], ["hya", "ヒャ"], ["hyu", "ヒュ"], ["hyo", "ヒョ"],
                               ["mya", "ミャ"], ["myu", "ミュ"], ["myo", "ミョ"], ["rya", "リャ"], ["ryu", "リュ"],
                               ["ryo", "リょ"], ["gya", "ギャ"], ["gyu", "ギュ"], ["gyo", "ギョ"], ["bya", "ビャ"],
                               ["byu", "ビュ"], ["byo", "ビョ"], ["pya", "ピャ"], ["pyu", "ピュ"], ["pyo", "ピョ"],
                               ["tsu", "ツ"], ["shi", "シ"], ["chi", "チ"], ["fyu", "フュ"], ["who", "ウォ"],
                               ["tsa", "ツァ"], ["tsi", "ツィ"], ["tse", "ツェ"], ["tso", "ツォ"], ["che", "チェ"],
                               ["she", "シェ"],
                               ["ka", "カ"], ["ki", "キ"], ["ku", "ク"], ["ke", "ケ"], ["ko", "コ"], ["sa", "サ"],
                               ["su", "ス"], ["se", "セ"], ["so", "ソ"], ["ta", "タ"], ["te", "テ"], ["to", "ト"],
                               ["na", "ナ"], ["ni", "ニ"], ["nu", "ヌ"], ["ne", "ネ"], ["no", "ノ"], ["ha", "ハ"],
                               ["hi", "ヒ"], ["fu", "フ"], ["he", "ヘ"], ["ho", "ホ"], ["ma", "マ"], ["mi", "ミ"],
                               ["mu", "ム"], ["me", "メ"], ["mo", "モ"], ["ya", "ヤ"], ["yu", "ユ"], ["yo", "ヨ"],
                               ["ra", "ラ"], ["ri", "リ"], ["ru", "ル"], ["re", "レ"], ["ro", "ロ"], ["wa", "ワ"],
                               ["wo", "ヲ"], ["nn", "ン"], ["ga", "ガ"], ["gi", "ギ"], ["gu", "グ"], ["ge", "ゲ"],
                               ["go", "ゴ"], ["za", "ザ"], ["ji", "ジ"], ["zu", "ズ"], ["ze", "ゼ"], ["zo", "ゾ"],
                               ["da", "ダ"], ["di", "ヂ"], ["du", "ヅ"], ["de", "デ"], ["do", "ド"], ["ba", "バ"],
                               ["bi", "ビ"], ["bu", "ブ"], ["be", "ベ"], ["bo", "ボ"], ["pa", "パ"], ["pi", "ピ"],
                               ["pu", "プ"], ["pe", "ペ"], ["po", "ポ"], ["ja", "ジャ"], ["ju", "ジュ"], ["jo", "ジョ"],
                               ["fa", "ファ"], ["fi", "フィ"], ["fe", "フェ"], ["fo", "フォ"], ["wi", "ウィ"],
                               ["we", "ウェ"], ["va", "ヴァ"], ["vi", "ヴィ"], ["ve", "ヴェ"], ["vo", "ヴォ"],
                               ["je", "ジェ"],
                               ["a", "ア"], ["i", "イ"], ["u", "ウ"], ["e", "エ"], ["o", "オ"]]

    # Private methods.
    def __init__(self, gui_keyboard):
        super().__init__()
        self.__gui = gui_keyboard
        self.__init_connects()
        self.__set_keys(self.__KEYS_RU)
        self.__set_keys_enabled(False)

    def __init_connects(self):
        for row in range(len(self.__gui.num_row)):
            for key in range(len(self.__gui.pb_keys[row])):
                self.__gui.pb_keys[row][key].clicked.connect(self.__key_clicked)
        self.__gui.pb_shift.pressed.connect(self.__shift_pressed)
        self.__gui.pb_shift.released.connect(self.__shift_released)
        self.__gui.pb_left.clicked.connect(self.__left_clicked)
        self.__gui.pb_right.clicked.connect(self.__right_clicked)
        self.__gui.pb_language.clicked.connect(self.__language_clicked)
        self.__gui.pb_space.clicked.connect(self.__space_clicked)
        self.__gui.pb_backspace.clicked.connect(self.__backspace_clicked)

    def __set_keys(self, keys):
        for row in range(len(self.__gui.num_row)):
            for key in range(len(self.__gui.pb_keys[row])):
                self.__gui.pb_keys[row][key].setText(keys[row][key])
                self.__gui.pb_keys[row][key].setEnabled(len(keys[row][key]) != 0)

    def __set_keys_enabled(self, enabled):
        for row in range(len(self.__gui.num_row)):
            for key in range(len(self.__gui.pb_keys[row])):
                self.__gui.pb_keys[row][key].setEnabled(enabled)
        self.__gui.pb_shift.setEnabled(enabled)
        self.__gui.pb_shift.setEnabled(enabled)
        self.__gui.pb_left.setEnabled(enabled)
        self.__gui.pb_right.setEnabled(enabled)
        self.__gui.pb_language.setEnabled(enabled)
        self.__gui.pb_space.setEnabled(enabled)
        self.__gui.pb_backspace.setEnabled(enabled)

    def __update_gui_access(self):
        pos = self.__line_edit.cursorPosition()
        t_len = len(self.__line_edit.text())
        self.__gui.pb_right.setEnabled(pos != t_len)
        self.__gui.pb_left.setEnabled(pos != 0)
        self.__gui.pb_backspace.setEnabled(pos != 0)

    def __convert_to_hiragana(self, text):
        for i in self.__JP_TO_HIRAGANA:
            text = text.replace(i[0], i[1])
        return text

    def __convert_to_katakana(self, text):
        for i in self.__JP_TO_KATAKANA:
            text = text.replace(i[0], i[1])
        return text

    def __set_line_text_safely(self, text, cursor_pos):
        self.__line_edit.cursorPositionChanged.disconnect(self.__cursor_pos_changed)
        self.__line_edit.setText(text)
        self.__line_edit.setCursorPosition(cursor_pos)
        self.__line_edit.cursorPositionChanged.connect(self.__cursor_pos_changed)

    def __set_line_pos_safely(self, cursor_pos):
        self.__line_edit.cursorPositionChanged.disconnect(self.__cursor_pos_changed)
        self.__line_edit.setCursorPosition(cursor_pos)
        self.__line_edit.cursorPositionChanged.connect(self.__cursor_pos_changed)

    def __add_char(self, char):
        p = self.__line_edit.cursorPosition()
        t = self.__line_edit.text()
        t = t[:p] + char + t[p:]
        if self.__gui.pb_language.text() == "JpH":
            t = self.__convert_to_hiragana(t)
        elif self.__gui.pb_language.text() == "JpK":
            t = self.__convert_to_katakana(t)
        self.__set_line_text_safely(t, p + 1)

    def __remove_char(self):
        p = self.__line_edit.cursorPosition()
        t = self.__line_edit.text()
        self.__set_line_text_safely(t[:p - 1] + t[p:], p - 1)

    # Private slots.
    def __key_clicked(self):
        self.__add_char(self.sender().text())
        self.__update_gui_access()

    def __shift_pressed(self):
        self.__set_keys(self.__KEYS_RU_SHIFT)

    def __shift_released(self):
        self.__set_keys(self.__KEYS_RU)

    def __left_clicked(self):
        self.__set_line_pos_safely(self.__line_edit.cursorPosition() - 1)
        self.__update_gui_access()

    def __right_clicked(self):
        self.__set_line_pos_safely(self.__line_edit.cursorPosition() + 1)
        self.__update_gui_access()

    def __language_clicked(self):
        if self.__gui.pb_language.text() == "Ru":
            self.__gui.pb_language.setText("JpH")
            self.__set_keys(self.__KEYS_JP_HIRAGANA)
            self.__gui.pb_shift.setEnabled(False)
        elif self.__gui.pb_language.text() == "JpH":
            self.__gui.pb_language.setText("JpK")
            self.__set_keys(self.__KEYS_JP_KATAKANA)
        else:
            self.__gui.pb_language.setText("Ru")
            self.__set_keys(self.__KEYS_RU)
            self.__gui.pb_shift.setEnabled(True)

    def __space_clicked(self):
        self.__add_char(" ")
        self.__update_gui_access()

    def __cursor_pos_changed(self):
        self.__update_gui_access()

    def __backspace_clicked(self):
        self.__remove_char()
        self.__update_gui_access()
