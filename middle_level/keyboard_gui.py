from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_disabled_square_button, get_enabled_button, get_enabled_square_button


class KeyboardGui(QWidget):
    __key_num = 45

    num_row = [11, 12, 11, 9]
    pb_keys = [[None] * num_row[0], [None] * num_row[1], [None] * num_row[2], [None] * num_row[3]]
    pb_shift = None
    pb_left = None
    pb_right = None
    pb_language = None
    pb_space = None
    pb_backspace = None

    def __init__(self):
        super().__init__()
        self.init_objects()
        self.init_gui()

    def init_objects(self):
        for row in range(len(self.num_row)):
            for key in range(len(self.pb_keys[row])):
                self.pb_keys[row][key] = get_disabled_square_button("", self.__key_num)
                self.pb_keys[row][key].setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_shift = get_enabled_button("Shift", self.__key_num)
        self.pb_left = get_enabled_square_button("Left", self.__key_num)
        self.pb_right = get_enabled_square_button("Right", self.__key_num)
        self.pb_language = get_enabled_button("Ru", self.__key_num)
        self.pb_space = get_enabled_button("Space", self.__key_num)
        self.pb_backspace = get_enabled_button("Back\nspace", self.__key_num)
        self.pb_shift.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_left.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_right.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_language.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_space.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.pb_backspace.setFocusPolicy(Qt.FocusPolicy.NoFocus)

    def init_gui(self):
        main_layout = QHBoxLayout(self)

        spacial_keys_layout = QVBoxLayout()
        main_layout.addLayout(spacial_keys_layout)
        spacial_keys_layout.addWidget(self.pb_shift)

        left_right_layout = QHBoxLayout()
        spacial_keys_layout.addLayout(left_right_layout)
        left_right_layout.addWidget(self.pb_left)
        left_right_layout.addWidget(self.pb_right)

        spacial_keys_layout.addWidget(self.pb_language)
        spacial_keys_layout.addWidget(self.pb_space)

        main_keys_layout = QVBoxLayout()
        main_layout.addLayout(main_keys_layout)
        for row in range(len(self.num_row)):
            row_layout = QHBoxLayout()
            main_keys_layout.addLayout(row_layout)
            row_layout.addStretch(1)
            for key in range(len(self.pb_keys[row])):
                row_layout.addWidget(self.pb_keys[row][key])
            if row == 3:
                row_layout.addWidget(self.pb_backspace)
            row_layout.addStretch(1)
