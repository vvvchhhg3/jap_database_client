from PyQt5.QtCore import QObject


class GrammarInfoLogic(QObject):
    # Public methods.
    def set(self, grammar):
        self.__gui.l_grammar.setText(grammar.jp + "\n\n" + grammar.translate)
        self.__gui.lw_examples.clear()
        for i in grammar.examples:
            self.__gui.lw_examples.addItem(i.jp + "\n\n" + i.furigana)

    def reset(self):
        self.__gui.l_grammar.clear()
        self.__gui.lw_examples.clear()

    # Private variables and constants.
    __gui = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        self.__gui = gui
