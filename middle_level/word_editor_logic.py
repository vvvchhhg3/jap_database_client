from PyQt5.QtCore import QObject, pyqtSignal

from low_level.item_types import word_item


class WordEditorLogic(QObject):
    # Public signals.
    replace = pyqtSignal(tuple, tuple)

    # Public methods.
    def set_word(self, word):
        self.__word = word
        self.__disconnect_connects()
        self.__clear()
        self.__set_default()
        self.__init_connects()
        self.__set_enabled(True)
        self.__update_gui_access()

    def reset_word(self):
        self.__word = None
        self.__disconnect_connects()
        self.__clear()
        self.__init_connects()
        self.__set_enabled(False)

    def set_tags(self, tags):
        self.__tags = tags
        self.__disconnect_connects()
        self.__update_tags()
        self.__init_connects()

    # Private variables and constants.
    __gui = None
    __is_word_exist = None
    __tags = None
    __word = None

    # Private methods.
    def __init__(self, gui_word_editor, is_word_exist):
        super().__init__()
        self.__gui = gui_word_editor
        self.__is_word_exist = is_word_exist
        self.__init_connects()

    def __init_connects(self):
        self.__gui.le_jp.textChanged.connect(self.__jp_changed)
        self.__gui.le_kanji.textChanged.connect(self.__kanji_changed)
        self.__init_tags_connects()
        for i in range(self.__gui.num_max_translate):
            self.__gui.le_translates[i].textChanged.connect(self.__translate_changed)
        self.__gui.pb_clear.clicked.connect(self.__clear_clicked)
        self.__gui.pb_save.clicked.connect(self.__save_clicked)
        self.__gui.pb_reset.clicked.connect(self.__reset_clicked)

    def __disconnect_connects(self):
        self.__gui.le_jp.textChanged.disconnect(self.__jp_changed)
        self.__gui.le_kanji.textChanged.disconnect(self.__kanji_changed)
        self.__disconnect_tags_connects()
        for i in range(self.__gui.num_max_translate):
            self.__gui.le_translates[i].textChanged.disconnect(self.__translate_changed)
        self.__gui.pb_clear.clicked.disconnect(self.__clear_clicked)
        self.__gui.pb_save.clicked.disconnect(self.__save_clicked)
        self.__gui.pb_reset.clicked.disconnect(self.__reset_clicked)

    def __init_tags_connects(self):
        for i in range(self.__gui.num_max_tag):
            self.__gui.cb_tags[i].currentIndexChanged.connect(self.__row_changed)

    def __disconnect_tags_connects(self):
        for i in range(self.__gui.num_max_tag):
            self.__gui.cb_tags[i].currentIndexChanged.disconnect(self.__row_changed)

    def __update_tags(self):
        tags_names = self.__get_current_tags_list()
        tags_list = [''] + self.__tags.copy()
        for i in range(self.__gui.num_max_tag):
            self.__gui.cb_tags[i].clear()
            self.__gui.cb_tags[i].addItems(tags_list)
            if i < len(tags_names):
                if tags_names[i] in tags_list:
                    self.__gui.cb_tags[i].setCurrentText(tags_names[i])
                    tags_list.remove(tags_names[i])

    def __is_tags_default(self):
        tags = self.__word.tags.copy()
        for i in range(self.__gui.num_max_tag):
            if self.__gui.cb_tags[i].currentIndex() != 0:
                i_tag = self.__gui.cb_tags[i].currentText()
                if i_tag in tags:
                    tags.remove(i_tag)
                else:
                    return False
        return len(tags) == 0

    def __is_translates_default(self):
        translate = self.__word.translate.copy()
        for i in range(self.__gui.num_max_translate):
            i_translate = self.__gui.le_translates[i].text()
            if len(i_translate) == 0:
                continue
            if i_translate in translate:
                translate.remove(i_translate)
            else:
                return False
        return len(translate) == 0

    def __is_default_word_state(self):
        if self.__gui.le_jp.text() != self.__word.jp:
            return False
        if self.__gui.le_kanji.text() != self.__word.kanji:
            return False
        if not self.__is_tags_default():
            return False
        if not self.__is_translates_default():
            return False
        return self.__is_translates_default()

    def __is_fields_cleared(self):
        if len(self.__gui.le_jp.text()) != 0:
            return False
        if len(self.__gui.le_kanji.text()) != 0:
            return False
        for i in range(self.__gui.num_max_tag):
            if self.__gui.cb_tags[i].currentIndex() != 0:
                return False
        for i in range(self.__gui.num_max_translate):
            if len(self.__gui.le_translates[i].text()) != 0:
                return False
        return True

    def __is_fields_valid(self):
        if len(self.__gui.le_jp.text()) == 0:
            return False
        valid = False
        for i in range(self.__gui.num_max_tag):
            if self.__gui.cb_tags[i].currentIndex() != 0:
                valid = True
                break
        if not valid:
            return False
        valid = False
        for i in range(self.__gui.num_max_translate):
            if len(self.__gui.le_translates[i].text()):
                valid = True
                break
        return valid

    def __is_word_empty_template(self):
        return (len(self.__word.jp) == 0) and (len(self.__word.kanji) == 0)

    def __update_gui_access(self):
        if self.__is_word_empty_template():
            self.__gui.pb_save.setEnabled(self.__is_fields_valid() and (not self.__is_default_word_state()) and
                                       (not self.__is_word_exist(self.__gui.le_jp.text(), self.__gui.le_kanji.text())))
        else:
            self.__gui.pb_save.setEnabled(self.__is_fields_valid() and (not self.__is_default_word_state()))

        self.__gui.pb_clear.setEnabled(not self.__is_fields_cleared())
        self.__gui.pb_reset.setEnabled(not self.__is_default_word_state())

    def __get_current_tags_list(self):
        tags_names = []
        for i in range(self.__gui.num_max_tag):
            text = self.__gui.cb_tags[i].currentText()
            if len(text) != 0:
                tags_names.append(text)
        return tags_names

    def __clear(self):
        self.__gui.le_jp.clear()
        self.__gui.le_kanji.clear()
        for i in range(self.__gui.num_max_tag):
            self.__gui.cb_tags[i].setCurrentIndex(0)
        for i in range(self.__gui.num_max_translate):
            self.__gui.le_translates[i].clear()

    def __set_default(self):
        self.__gui.le_jp.setText(self.__word.jp)
        self.__gui.le_kanji.setText(self.__word.kanji)
        for i in range(self.__gui.num_max_tag):
            if i >= len(self.__word.tags):
                break
            self.__gui.cb_tags[i].setCurrentText(self.__word.tags[i])
        for i in range(self.__gui.num_max_translate):
            if i >= len(self.__word.translate):
                break
            self.__gui.le_translates[i].setText(self.__word.translate[i])

    def __get_translations(self):
        translates = []
        for i in range(self.__gui.num_max_translate):
            i_str = self.__gui.le_translates[i].text()
            if len(i_str) == 0:
                continue
            translates.append(i_str)
        return translates

    def get_tags(self):
        tags = []
        for i in range(self.__gui.num_max_tag):
            i_str = self.__gui.cb_tags[i].itemText(self.__gui.cb_tags[i].currentIndex())
            if len(i_str) == 0:
                continue
            tags.append(i_str)
        return tags

    def __get_word(self):
        return word_item(self.__word.w_id, self.__gui.le_jp.text(), self.__gui.le_kanji.text(), self.get_tags(),
                         self.__get_translations())

    def __set_enabled(self, enabled):
        self.__gui.le_jp.setEnabled(enabled)
        self.__gui.le_kanji.setEnabled(enabled)
        for i in range(self.__gui.num_max_tag):
            self.__gui.cb_tags[i].setEnabled(enabled)
        for i in range(self.__gui.num_max_translate):
            self.__gui.le_translates[i].setEnabled(enabled)

    # Private slots.
    def __jp_changed(self, text):
        clear_text = text.replace("\n", "")
        if clear_text != text:
            self.__gui.le_jp.textChanged.disconnect(self.__jp_changed)
            self.__gui.le_jp.setText(clear_text)
            self.__gui.le_jp.textChanged.connect(self.__jp_changed)
        self.__update_gui_access()

    def __kanji_changed(self, text):
        clear_text = text.replace("\n", "")
        if clear_text != text:
            self.__gui.le_kanji.textChanged.disconnect(self.__kanji_changed)
            self.__gui.le_kanji.setText(clear_text)
            self.__gui.le_kanji.textChanged.connect(self.__kanji_changed)
        self.__update_gui_access()

    def __row_changed(self):
        self.__disconnect_tags_connects()
        self.__update_tags()
        self.__init_tags_connects()
        self.__update_gui_access()

    def __translate_changed(self, text):
        clear_text = text.replace("\n", "")
        if clear_text != text:
            self.sender().textChanged.disconnect(self.__translate_changed)
            self.sender().setText(clear_text)
            self.sender().textChanged.connect(self.__translate_changed)
        self.__update_gui_access()

    def __clear_clicked(self):
        self.__disconnect_connects()
        self.__clear()
        self.__init_connects()
        self.__update_gui_access()

    def __reset_clicked(self):
        self.__disconnect_connects()
        self.__clear()
        self.__set_default()
        self.__init_connects()
        self.__update_gui_access()

    def __save_clicked(self):
        now = self.__get_word()
        old = self.__word
        self.__word = now
        self.__update_gui_access()
        self.replace.emit(old, now)
