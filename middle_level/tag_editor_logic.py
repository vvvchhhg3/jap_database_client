from PyQt5.QtCore import pyqtSignal

from low_level.item_name_editor_logic import ItemNameEditorLogic


class TagEditorLogic(ItemNameEditorLogic):
    # Public signals.
    replace = pyqtSignal(str, str)

    # Protected virtual functions.
    def _replace_name(self, old, new):
        self.replace.emit(old, new)
