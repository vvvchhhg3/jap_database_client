from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from middle_level.word_info_logic import WordInfoLogic
from middle_level.word_list_viewer_logic import WordListViewerLogic


class TabViewUserWordListLogic(QObject):
    # Public methods.
    def reshow(self):
        if DataController().user_word_list.get_list_len() != 0:
            self.__update_lists(DataController().user_word_list.get_sorted_name_list())
            name = self.__gui.lists.currentText()
            self.__list.set(DataController().user_word_list.get_sorted_list(name))
            self.__gui.lists.setEnabled(True)
        else:
            self.__list.unselect()
            self.__gui.lists.clear()
            self.__gui.lists.setEnabled(False)

    # Private variables and constants.
    __gui = None
    __list = None
    __info = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__gui = gui
        # Init GUI.
        self.__list = WordListViewerLogic(gui.list)
        self.__info = WordInfoLogic(gui.info)
        # Init connects.
        self.__list.selected.connect(self.__info.set)
        self.__list.unselected.connect(self.__info.reset)
        self.__gui.lists.currentIndexChanged.connect(self.__row_changed)
        # Start.
        self.reshow()

    def __update_lists(self, lists):
        cur_text = self.__gui.lists.currentText()
        self.__gui.lists.currentIndexChanged.disconnect(self.__row_changed)
        self.__gui.lists.clear()
        self.__gui.lists.addItems(lists)
        self.__gui.lists.setCurrentText(cur_text)
        self.__gui.lists.currentIndexChanged.connect(self.__row_changed)

    # Private slots.
    def __row_changed(self):
        self.reshow()
