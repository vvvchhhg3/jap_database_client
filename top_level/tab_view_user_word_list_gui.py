from PyQt5.QtWidgets import QWidget, QHBoxLayout, QSizePolicy, QVBoxLayout

from low_level.funcs_gui import get_disabled_combobox
from low_level.list_viewer_gui import ListViewerGui
from middle_level.word_info_gui import WordInfoGui


class TabViewUserWordListGui(QWidget):
    list = None
    info = None
    lists = None

    def __init__(self):
        # Init objects.
        super().__init__()
        self.lists = get_disabled_combobox()
        self.list = ListViewerGui()
        self.info = WordInfoGui()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        left_part = QVBoxLayout()
        l_main.addLayout(left_part, 1)
        left_part.addWidget(self.lists)
        left_part.addWidget(self.list)
        l_main.addWidget(self.info, 3)
