from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_disabled_checkbox, get_center_label, get_disabled_button, \
    get_disabled_combobox


class TabRandomWordGui(QWidget):
    cb_jp = [None] * 2
    cb_kanji = [None] * 2
    cb_translate = [None] * 2
    cb_tags = [None] * 2
    pb_word = None
    pb_next = None
    cb_tag = None
    l_button = None

    def __init__(self):
        super().__init__()
        # Init objects.
        for i in range(2):
            self.cb_jp[i] = get_disabled_checkbox("Jp")
            self.cb_kanji[i] = get_disabled_checkbox("Kanji")
            self.cb_translate[i] = get_disabled_checkbox("Translate")
            self.cb_tags[i] = get_disabled_checkbox("Tags")
        self.pb_word = get_disabled_button("", -1)
        self.l_button = get_center_label("")
        self.l_button.setTextInteractionFlags(Qt.NoTextInteraction)
        self.l_button.setWordWrap(True)
        self.l_button.setMouseTracking(False)
        font = self.l_button.font()
        font.setPointSize(40)
        self.l_button.setFont(font)
        self.pb_next = get_disabled_button("Next", 80)
        self.cb_tag = get_disabled_combobox()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        cfg_layout = QVBoxLayout()
        l_main.addLayout(cfg_layout, 1)
        cfg_layout.addWidget(get_center_label("Tag"))
        cfg_layout.addWidget(self.cb_tag)
        for i in range(2):
            cfg_layout.addWidget(get_center_label("Side " + str(i + 1)))
            cfg_layout.addWidget(self.cb_jp[i])
            cfg_layout.addWidget(self.cb_kanji[i])
            cfg_layout.addWidget(self.cb_translate[i])
            cfg_layout.addWidget(self.cb_tags[i])
        cfg_layout.addStretch(1)
        game_layout = QVBoxLayout()
        l_main.addLayout(game_layout, 2)
        game_layout.addWidget(self.pb_word)
        l_word_button = QHBoxLayout()
        self.pb_word.setLayout(l_word_button)
        l_word_button.addWidget(self.l_button)
        game_layout.addWidget(self.pb_next)
