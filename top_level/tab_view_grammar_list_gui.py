from PyQt5.QtWidgets import QWidget, QHBoxLayout, QSizePolicy

from low_level.list_viewer_gui import ListViewerGui
from middle_level.grammar_info_gui import GrammarInfoGui


class TabViewGrammarListGui(QWidget):
    list = None
    info = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.list = ListViewerGui()
        self.info = GrammarInfoGui()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        l_main.addWidget(self.list, 1)
        l_main.addWidget(self.info, 3)
