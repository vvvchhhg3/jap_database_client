from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QWidget, QHBoxLayout, QListWidgetItem, QFrame
from middle_level.keyboard_gui import KeyboardGui
from low_level.funcs_gui import get_list_widget
from top_level.tab_training_cards_gui import TabTrainingCardsGui
from top_level.tab_view_grammar_list_gui import TabViewGrammarListGui
from top_level.tab_random_grammar_gui import TabRandomGrammarGui
from top_level.tab_random_word_gui import TabRandomWordGui
from top_level.tab_edit_tag_list_gui import TabEditTagListGui
from top_level.tab_edit_common_word_list_gui import TabEditCommonWordListGui
from top_level.tab_view_common_word_list_gui import TabViewCommonWordListGui
from top_level.tab_edit_user_word_list_gui import TabEditUserWordListGui
from top_level.tab_view_user_word_list_gui import TabViewUserWordListGui


class JapDatabaseClientGui(QMainWindow):
    tab_view_common_word_list = None
    tab_edit_common_word_list = None
    tab_edit_tag_list = None
    tab_view_user_word_list = None
    tab_edit_user_word_list = None
    tab_random_word = None
    tab_random_grammar = None
    tab_training_cards = None

    map = None

    lw_menu_items = None
    lw_sub_menu_items = None

    w_shown_item = None
    keyboard = None

    def __init__(self):
        super().__init__()

        self.keyboard = KeyboardGui()

        self.tab_view_common_word_list = TabViewCommonWordListGui()
        self.tab_view_user_word_list = TabViewUserWordListGui()
        self.tab_view_grammar_list = TabViewGrammarListGui()

        self.tab_edit_common_word_list = TabEditCommonWordListGui()
        self.tab_edit_tag_list = TabEditTagListGui()
        self.tab_edit_user_word_list = TabEditUserWordListGui()

        self.tab_random_word = TabRandomWordGui()
        self.tab_random_grammar = TabRandomGrammarGui()

        self.tab_training_cards = TabTrainingCardsGui()

        # Init objects.
        # Menu:
        # ["Manu item name", "Path to icon for menu row", [Submenu items], Current submenu item]
        # Submenu item:
        # ["Submenu name", QWidget of submenu item]
        self.map = [
            ["View", "icons/row_view.png", [
                ["Common\nword list", self.tab_view_common_word_list],
                ["User\nword list", self.tab_view_user_word_list],
                ["Grammar list", self.tab_view_grammar_list]], 0],
            ["Edit", "icons/row_edit.png", [
                ["Tag list", self.tab_edit_tag_list],
                ["Common\nword list", self.tab_edit_common_word_list],
                ["User\nword list", self.tab_edit_user_word_list]], 0
             ],
            ["Training", "icons/row_training.png", [
                ["Cards", self.tab_training_cards]], 0],
            ["Random", "icons/row_random.png", [
                ["Word", self.tab_random_word],
                ["Grammar", self.tab_random_grammar]], 0]
        ]

        self.lw_menu_items = get_list_widget(20)
        for i in self.map:
            self.lw_menu_items.addItem(QListWidgetItem(QIcon(i[1]), i[0]))
            self.lw_menu_items.setIconSize(QSize(35, 35))

        self.lw_sub_menu_items = get_list_widget(20)
        for i in self.map[0][2]:
            self.lw_sub_menu_items.addItem(QListWidgetItem(i[0]))

        # Add all widgets and hide.
        l_shown_item = QVBoxLayout()
        for menu_item in self.map:
            for submenu_item in menu_item[2]:
                widget = submenu_item[1]
                widget.hide()
                l_shown_item.addWidget(widget)

        # Set default widget and row.
        self.lw_menu_items.setCurrentRow(0)
        self.lw_sub_menu_items.setCurrentRow(0)
        self.w_shown_item = self.map[0][2][0][1]
        self.w_shown_item.show()

        # Init GUI.
        self.setWindowTitle("JapDatabaseClient")
        self.setMinimumSize(1280, 720)

        w_main = QWidget()
        self.setCentralWidget(w_main)

        l_main = QHBoxLayout(w_main)

        l_main.addWidget(self.lw_menu_items, 2)
        l_main.addWidget(self.lw_sub_menu_items, 2)

        fr = QFrame()
        fr.setFrameShape(QFrame.StyledPanel)
        l_main.addWidget(fr, 12)
        fr.setLayout(l_shown_item)
