from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from middle_level.grammar_info_logic import GrammarInfoLogic
from middle_level.grammar_list_viewer_logic import GrammarListViewerLogic


class TabViewGrammarListLogic(QObject):
    # Public methods.
    def reshow(self):
        self.__list.set(DataController().grammar_list.get_list())

    # Private variables and constants.
    __list = None
    __info = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__list = GrammarListViewerLogic(gui.list)
        self.__info = GrammarInfoLogic(gui.info)
        # Init connects.
        self.__list.selected.connect(self.__info.set)
        self.__list.unselected.connect(self.__info.reset)
        # Start
        self.reshow()
