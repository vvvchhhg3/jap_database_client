from PyQt5.QtCore import pyqtSignal, QObject

from low_level.data_controller import DataController
from middle_level.tag_editor_logic import TagEditorLogic
from middle_level.tag_list_editor_logic import TagListEditorLogic


class TabEditTagListLogic(QObject):
    # Public signals.
    saved = pyqtSignal()

    # Public methods.
    def reshow(self):
        self.__list.set(DataController().tag_list.get_sorted_list())

    # Private variables and constants.
    __list = None
    __editor = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__list = TagListEditorLogic(gui.list)
        self.__editor = TagEditorLogic(gui.editor, DataController().tag_list.is_tag_exist)
        # Init connects.
        self.__list.selected.connect(self.__editor.set)
        self.__list.unselected.connect(self.__editor.reset)
        self.__list.saved.connect(self.__saved)
        self.__editor.replace.connect(self.__replace)
        # Start.
        self.reshow()

    # Private slots.
    def __saved(self):
        self.saved.emit()

    def __replace(self, old, new):
        DataController().tag_list.replace_tag(old, new)
        self.reshow()
        self.__list.select(new)
        self.saved.emit()
