from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

from low_level.funcs_gui import get_center_label, get_disabled_button, get_list_widget, get_big_center_label
from middle_level.random_column_word_gui import RandomColumnWordGui


class TabRandomGrammarGui(QWidget):
    w_words = [None] * 2
    l_grammar_handler = None
    lw_examples = None
    pb_next = None

    def __init__(self):
        super().__init__()
        # Init objects.
        for i in range(2):
            self.w_words[i] = RandomColumnWordGui()
        self.l_grammar_handler = get_big_center_label("")
        self.lw_examples = get_list_widget(20)
        self.pb_next = get_disabled_button("Next", 80)
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        word_layout = QHBoxLayout()
        l_main.addLayout(word_layout)
        for i in range(2):
            word_layout.addWidget(self.w_words[i])
        l_main.addWidget(get_center_label("Grammar"))
        l_main.addWidget(self.l_grammar_handler)
        l_main.addWidget(get_center_label("Examples"))
        l_main.addWidget(self.lw_examples)
        l_main.addWidget(self.pb_next)
