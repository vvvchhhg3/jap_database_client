from PyQt5.QtWidgets import QWidget, QHBoxLayout

from low_level.list_editor_gui import ListEditorGui
from middle_level.word_editor_gui import WordEditorGui


class TabEditCommonWordListGui(QWidget):
    list = None
    editor = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.list = ListEditorGui()
        self.editor = WordEditorGui()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        l_main.addWidget(self.list, 1)
        l_main.addWidget(self.editor, 3)
