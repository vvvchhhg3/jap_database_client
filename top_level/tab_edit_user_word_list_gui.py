from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout
from low_level.funcs_gui import get_disabled_combobox, get_disabled_button, get_center_label
from low_level.item_name_editor_gui import ItemNameEditorGui
from low_level.list_editor_gui import ListEditorGui
from low_level.list_viewer_gui import ListViewerGui


class TabEditUserWordListGui(QWidget):
    list_lists = None
    list_name_editor = None

    added_word_list = None
    tags = None
    free_word_list = None
    pb_add = None
    pb_remove = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.list_lists = ListEditorGui()
        self.list_name_editor = ItemNameEditorGui()
        self.added_word_list = ListViewerGui()
        self.tags = get_disabled_combobox()
        self.free_word_list = ListViewerGui()
        self.pb_add = get_disabled_button("Add")
        self.pb_remove = get_disabled_button("Remove")
        # Init GUI.
        l_main = QVBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        list_layout = QHBoxLayout()
        l_main.addLayout(list_layout)
        list_layout.addWidget(self.list_lists, 1)
        list_layout.addWidget(self.list_name_editor, 1)

        bottom_layout = QHBoxLayout()
        l_main.addLayout(bottom_layout, 1)

        left_part = QVBoxLayout()
        bottom_layout.addLayout(left_part, 1)
        left_part.addWidget(self.added_word_list)
        left_part.addWidget(self.pb_remove)

        right_part = QVBoxLayout()
        bottom_layout.addLayout(right_part, 1)
        right_part.addWidget(self.tags)
        right_part.addWidget(self.free_word_list)
        right_part.addWidget(self.pb_add)
