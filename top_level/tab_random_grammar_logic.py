import random

from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from middle_level.random_column_word_logic import RandomColumnWordLogic


class TabRandomGrammarLogic(QObject):
    # Public methods.
    def reshow(self):
        words = DataController().common_word_list.get_sorted_list()
        tags = DataController().tag_list.get_sorted_list()
        for i in range(2):
            self.__random_word[i].set(words, tags)
        self.__grammars = DataController().grammar_list.get_list()
        self.__set_random_grammars()
        self.__gui.pb_next.setEnabled((len(words) is not None) and
                                      (len(tags) is not None) and
                                      (len(self.__grammars) is not None))

    # Private variables and constants.
    __gui = None
    __random_word = [None] * 2
    __grammars = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__side_num = 0
        self.__gui = gui
        for i in range(2):
            self.__random_word[i] = RandomColumnWordLogic(self.__gui.w_words[i])
        # Init connects.
        self.__gui.pb_next.clicked.connect(self.__next_clicked)
        # Start.
        self.reshow()

    def __set_random_grammars(self):
        if len(self.__grammars) == 0:
            return
        grammar = random.choice(self.__grammars)
        self.__gui.l_grammar_handler.setText(grammar.jp + "\n" + grammar.translate)
        self.__gui.lw_examples.clear()
        for i in grammar.examples:
            self.__gui.lw_examples.addItem(i.jp + "\n\n" + i.furigana)

    # Private slots.
    def __next_clicked(self):
        self.__set_random_grammars()
        for i in range(2):
            self.__random_word[i].update()
