from PyQt5.QtWidgets import QWidget, QHBoxLayout

from low_level.list_viewer_gui import ListViewerGui
from middle_level.word_info_gui import WordInfoGui


class TabViewCommonWordListGui(QWidget):
    list = None
    info = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.list = ListViewerGui()
        self.info = WordInfoGui()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        l_main.addWidget(self.list, 1)
        l_main.addWidget(self.info, 3)
