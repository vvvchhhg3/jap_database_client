import random

from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from low_level.random_word import RandomWord


class TabRandomWordLogic(QObject):
    # Public methods.
    def reshow(self):
        tags = DataController().tag_list.get_sorted_list()
        if len(tags) == 0:
            self.__gui.cb_tag.setEnabled(False)
        else:
            self.__update_tags(tags)
            self.__gui.cb_tag.setEnabled(True)
            self.__random_word.set_words(DataController().common_word_list.get_sorted_list())
            self.__random_word.set_tag("")
            self.__random_word.get_current_word()
            self.__set_button_text()
        self.__update_gui_access()

    # Private variables and constants.
    __gui = None
    __side_num = None
    __random_word = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__side_num = 0
        self.__gui = gui
        self.__random_word = RandomWord()
        self.__gui.cb_jp[0].setChecked(True)
        self.__gui.cb_translate[1].setChecked(True)
        # Init connects.
        for i in range(2):
            self.__gui.cb_jp[i].stateChanged.connect(self.__checkbox_state_changed)
            self.__gui.cb_kanji[i].stateChanged.connect(self.__checkbox_state_changed)
            self.__gui.cb_translate[i].stateChanged.connect(self.__checkbox_state_changed)
            self.__gui.cb_tags[i].stateChanged.connect(self.__checkbox_state_changed)
        self.__gui.cb_tag.currentIndexChanged.connect(self.__row_changed)
        self.__gui.pb_word.clicked.connect(self.__word_clicked)
        self.__gui.pb_next.clicked.connect(self.__next_clicked)
        # Start.
        self.reshow()

    def __get_button_string(self, word, side_num):
        s = "Side " + str(side_num + 1) + "\n\n"
        if self.__gui.cb_jp[side_num].isChecked():
            s += word.jp + "\n\n"
        if self.__gui.cb_kanji[side_num].isChecked() and (len(word.kanji) != 0):
            s += word.kanji + "\n\n"
        if self.__gui.cb_translate[side_num].isChecked():
            for i in word.translate:
                s += i + "\n"
            s += "\n"
        if self.__gui.cb_tags[side_num].isChecked():
            for i in word.tags:
                s += i + "\n"
            s += "\n"
        return s

    def __set_button_text(self):
        self.__gui.l_button.setText(self.__get_button_string(self.__random_word.get_current_word(), self.__side_num))

    def __update_tags(self, tags):
        cur_text = self.__gui.cb_tag.currentText()
        self.__gui.cb_tag.currentIndexChanged.disconnect(self.__row_changed)
        self.__gui.cb_tag.clear()
        self.__gui.cb_tag.addItems([""] + tags)
        self.__gui.cb_tag.setCurrentText(cur_text)
        self.__gui.cb_tag.currentIndexChanged.connect(self.__row_changed)

    def __set_enabled(self, enabled):
        for i in range(2):
            self.__gui.cb_jp[i].setEnabled(enabled)
            self.__gui.cb_kanji[i].setEnabled(enabled)
            self.__gui.cb_translate[i].setEnabled(enabled)
            self.__gui.cb_tags[i].setEnabled(enabled)
        self.__gui.pb_word.setEnabled(enabled)
        self.__gui.pb_next.setEnabled(enabled)

    def __update_gui_access(self):
        self.__set_enabled(self.__random_word.get_current_word() is not None)

    # Private slots.
    def __row_changed(self):
        self.__side_num = 0
        self.__random_word.set_tag(self.__gui.cb_tag.currentText())
        if self.__random_word.get_current_word() is not None:
            self.__set_button_text()
        else:
            self.__gui.l_button.clear()
        self.__update_gui_access()

    def __word_clicked(self):
        if self.__side_num == 0:
            self.__side_num = 1
        else:
            self.__side_num = 0
        self.__set_button_text()

    def __checkbox_state_changed(self):
        self.__set_button_text()

    def __next_clicked(self):
        self.__side_num = 0
        self.__random_word.update()
        self.__set_button_text()
