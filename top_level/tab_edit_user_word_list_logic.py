from PyQt5.QtCore import pyqtSignal, QObject

from low_level.data_controller import DataController
from middle_level.list_name_editor_logic import ListNameEditorLogic
from middle_level.list_user_word_list_name_editor_logic import ListUserWordListNameEditorLogic
from middle_level.word_list_viewer_logic import WordListViewerLogic


class TabEditUserWordListLogic(QObject):
    # Public signals.
    saved = pyqtSignal()

    # Public methods.
    def reshow(self):
        self.__list_name.set(DataController().user_word_list.get_sorted_name_list())
        self.__update_tags()

    # Private variables and constants.
    __gui = None

    __list_name = None
    __list_name_editor = None
    __added_words = None
    __free_words = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__gui = gui
        self.__list_name = ListUserWordListNameEditorLogic(gui.list_lists)
        self.__list_name_editor = ListNameEditorLogic(gui.list_name_editor,
                                                      DataController().user_word_list.is_list_exist)
        self.__added_words = WordListViewerLogic(gui.added_word_list)
        self.__free_words = WordListViewerLogic(gui.free_word_list)
        # Init connects.
        self.__list_name.selected.connect(self.__list_name_editor.set)
        self.__list_name.selected.connect(self.__select)
        self.__list_name.unselected.connect(self.__list_name_editor.reset)
        self.__list_name.unselected.connect(self.__unselect)
        self.__list_name.saved.connect(self.__saved)
        self.__list_name_editor.replace.connect(self.__replace)
        self.__added_words.selected.connect(self.__added_word_selected)
        self.__added_words.unselected.connect(self.__added_word_unselected)
        self.__free_words.selected.connect(self.__free_word_selected)
        self.__free_words.unselected.connect(self.__free_word_unselected)
        self.__gui.tags.currentIndexChanged.connect(self.__row_changed)
        self.__gui.pb_add.clicked.connect(self.__add_clicked)
        self.__gui.pb_remove.clicked.connect(self.__remove_clicked)
        # Start.
        self.reshow()

    def __reset_current_tag(self):
        self.__gui.tags.currentIndexChanged.disconnect(self.__row_changed)
        self.__gui.tags.setCurrentIndex(0)
        self.__gui.tags.currentIndexChanged.connect(self.__row_changed)

    def __update_tags(self):
        cur_text = self.__gui.tags.currentText()
        self.__gui.tags.currentIndexChanged.disconnect(self.__row_changed)
        self.__gui.tags.clear()
        self.__gui.tags.addItems([""] + DataController().tag_list.get_sorted_list())
        self.__gui.tags.setCurrentText(cur_text)
        self.__gui.tags.currentIndexChanged.connect(self.__row_changed)

    def __update_word_lists(self):
        self.__added_words.set(DataController().user_word_list.get_sorted_list(self.__get_current_list_name()))
        self.__free_words.set(DataController().user_word_list.get_sorted_free_list(self.__get_current_list_name(),
                                                                                   self.__gui.tags.currentText()))

    def __get_current_list_name(self):
        return self.__list_name.get_current_item()

    # Private slots.
    def __saved(self):
        self.saved.emit()

    def __replace(self, old, new):
        DataController().user_word_list.replace_name(old, new)
        DataController().user_word_list.save()
        self.__list_name.set(DataController().user_word_list.get_sorted_name_list())
        self.__list_name.select(new)
        self.saved.emit()

    def __row_changed(self):
        self.__free_words.set(DataController().user_word_list.get_sorted_free_list(
            self.__get_current_list_name(), self.__gui.tags.currentText()))

    def __added_word_selected(self):
        self.__gui.pb_remove.setEnabled(True)

    def __added_word_unselected(self):
        self.__gui.pb_remove.setEnabled(False)

    def __free_word_selected(self):
        self.__gui.pb_add.setEnabled(True)

    def __free_word_unselected(self):
        self.__gui.pb_add.setEnabled(False)

    def __add_clicked(self):
        DataController().user_word_list.add_word(self.__list_name.get_current_item(),
                                                 self.__free_words.get_current_item())
        DataController().user_word_list.save()
        self.__update_word_lists()
        self.saved.emit()

    def __remove_clicked(self):
        DataController().user_word_list.remove_word(self.__list_name.get_current_item(),
                                                    self.__added_words.get_current_item())
        DataController().user_word_list.save()
        self.__update_word_lists()
        self.saved.emit()

    def __select(self):
        self.__reset_current_tag()
        self.__gui.tags.setEnabled(True)
        self.__update_word_lists()

    def __unselect(self):
        self.__added_words.unselect()
        self.__free_words.unselect()
        self.__gui.tags.setEnabled(False)
        self.__gui.pb_add.setEnabled(False)
        self.__gui.pb_remove.setEnabled(False)
