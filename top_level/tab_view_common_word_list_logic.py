from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from middle_level.word_info_logic import WordInfoLogic
from middle_level.word_list_viewer_logic import WordListViewerLogic


class TabViewCommonWordListLogic(QObject):
    # Public methods.
    def reshow(self):
        self.__list.set(DataController().common_word_list.get_sorted_list())

    # Private variables and constants.
    __list = None
    __info = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__list = WordListViewerLogic(gui.list)
        self.__info = WordInfoLogic(gui.info)
        # Init connects.
        self.__list.selected.connect(self.__info.set)
        self.__list.unselected.connect(self.__info.reset)
        # Start
        self.reshow()
