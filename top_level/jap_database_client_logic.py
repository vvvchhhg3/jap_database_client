from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QListWidgetItem

from low_level.data_controller import DataController

from top_level.tab_training_cards_logic import TabTrainingCardsLogic
from top_level.tab_view_grammar_list_logic import TabViewGrammarListLogic
from top_level.tab_random_grammar_logic import TabRandomGrammarLogic
from top_level.tab_random_word_logic import TabRandomWordLogic
from top_level.tab_edit_tag_list_logic import TabEditTagListLogic
from top_level.tab_edit_common_word_list_logic import TabEditCommonWordListLogic
from top_level.tab_view_common_word_list_logic import TabViewCommonWordListLogic
from top_level.jap_database_client_gui import JapDatabaseClientGui
from top_level.tab_edit_user_word_list_logic import TabEditUserWordListLogic
from top_level.tab_view_user_word_list_logic import TabViewUserWordListLogic


class JapDatabaseClient(QObject):
    # Private variables and constants.
    __gui = None

    __tab_view_common_word_list = None
    __tab_edit_common_word_list = None

    __tab_view_user_word_list = None
    __tab_edit_user_word_list = None

    __tab_edit_tag_list = None

    __tab_view_grammar_list = None

    __tab_random_word = None
    __tab_random_grammar = None

    __tab_training_cards = None

    __data_controller = None

    # Private methods.
    def __init__(self):
        super().__init__()
        self.__data_controller = DataController()

        self.__gui = JapDatabaseClientGui()

        self.__tab_view_common_word_list = TabViewCommonWordListLogic(self.__gui.tab_view_common_word_list)
        self.__tab_view_user_word_list = TabViewUserWordListLogic(self.__gui.tab_view_user_word_list)
        self.__tab_view_grammar_list = TabViewGrammarListLogic(self.__gui.tab_view_grammar_list)

        self.__tab_edit_common_word_list = TabEditCommonWordListLogic(self.__gui.tab_edit_common_word_list)
        self.__tab_edit_tag_list = TabEditTagListLogic(self.__gui.tab_edit_tag_list)
        self.__tab_edit_user_word_list = TabEditUserWordListLogic(self.__gui.tab_edit_user_word_list)

        self.__tab_random_word = TabRandomWordLogic(self.__gui.tab_random_word)
        self.__tab_random_grammar = TabRandomGrammarLogic(self.__gui.tab_random_grammar)

        self.__tab_training_cards = TabTrainingCardsLogic(self.__gui.tab_training_cards)

        self.__init_connects()
        self.__gui.show()

    def __init_connects(self):
        self.__gui.lw_menu_items.currentRowChanged.connect(self.__menu_items_row_changed)
        self.__gui.lw_sub_menu_items.currentRowChanged.connect(self.__sub_menu_items_row_changed)
        self.__tab_edit_common_word_list.saved.connect(self.__saved_common_words)
        self.__tab_edit_tag_list.saved.connect(self.__saved_tag_list)
        self.__tab_edit_user_word_list.saved.connect(self.__saved_user_word_list)

    def __set_submenu_widget(self, menu_row, submenu_row):
        self.__gui.w_shown_item.hide()
        self.__gui.w_shown_item = self.__gui.map[menu_row][2][submenu_row][1]
        self.__gui.w_shown_item.show()

    # Private slots.
    def __menu_items_row_changed(self, row):
        submenu_row = self.__gui.map[row][3]
        self.__set_submenu_widget(row, submenu_row)
        self.__gui.lw_sub_menu_items.currentRowChanged.disconnect(self.__sub_menu_items_row_changed)
        self.__gui.lw_sub_menu_items.clear()
        for i in self.__gui.map[row][2]:
            self.__gui.lw_sub_menu_items.addItem(QListWidgetItem(i[0]))
        self.__gui.lw_sub_menu_items.setCurrentRow(submenu_row)
        self.__gui.lw_sub_menu_items.currentRowChanged.connect(self.__sub_menu_items_row_changed)

    def __sub_menu_items_row_changed(self, row):
        menu_item_row = self.__gui.lw_menu_items.currentRow()
        self.__gui.map[menu_item_row][3] = row
        self.__set_submenu_widget(menu_item_row, row)

    def __saved_common_words(self):
        self.__tab_view_common_word_list.reshow()

    def __saved_tag_list(self):
        self.__tab_view_common_word_list.reshow()
        self.__tab_edit_common_word_list.reshow()
        self.__tab_edit_user_word_list.reshow()

    def __saved_user_word_list(self):
        self.__tab_training_cards.reshow()
