import random

from PyQt5.QtCore import QObject

from low_level.data_controller import DataController
from low_level.training_controller import TrainingController
from middle_level.training_cards_logic import TrainingCardsLogic


class TabTrainingCardsLogic(QObject):
    # Public methods.
    def reshow(self):
        lists = DataController().user_word_list.get_sorted_name_list()
        self.__gui.cb_list.clear()
        if len(lists) != 0:
            self.__gui.cb_list.addItems(lists)
        self.__set_enabled(len(lists) != 0)

    # Private variables and constants.
    __gui = None
    __tc = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__gui = gui
        self.__tc = TrainingCardsLogic(self.__gui.tc)
        self.__gui.cb_num.addItems(["5", "10", "15", "20", "25", "30"])
        self.__gui.cb_type.addItems(["JpToKanji", "KanjiToJp", "JpToTranslate",
                                     "TranslateToJp", "KanjiToTranslate", "TranslateToKanji"])
        # Init connects.
        self.__gui.pb_start.clicked.connect(self.__start_clicked)
        self.__tc.ended.connect(self.__tc_ended)
        # Start
        self.reshow()

    def __set_enabled(self, enabled):
        self.__gui.cb_list.setEnabled(enabled)
        self.__gui.cb_num.setEnabled(enabled)
        self.__gui.cb_type.setEnabled(enabled)
        self.__gui.tc.setEnabled(enabled)
        self.__gui.pb_start.setEnabled(enabled)

    def __start_training(self, t_type):
        words = DataController().user_word_list.get_list(self.__gui.cb_list.currentText())
        words = sorted(words, key=lambda i: DataController().training.get_word_rating(i.w_id, t_type))
        words = words[0:int(self.__gui.cb_num.currentText())]
        random.shuffle(words)
        self.__tc.set(t_type, words)

    # Private slots.
    def __start_clicked(self):
        self.__gui.pb_start.setEnabled(False)
        match self.__gui.cb_type.currentIndex():
            case 0:
                self.__start_training(TrainingController.Type.JpToKanji)
            case 1:
                self.__start_training(TrainingController.Type.KanjiToJp)
            case 2:
                self.__start_training(TrainingController.Type.JpToTranslate)
            case 3:
                self.__start_training(TrainingController.Type.TranslateToJp)
            case 4:
                self.__start_training(TrainingController.Type.KanjiToTranslate)
            case 5:
                self.__start_training(TrainingController.Type.TranslateToKanji)

    def __tc_ended(self):
        self.__gui.pb_start.setEnabled(True)
