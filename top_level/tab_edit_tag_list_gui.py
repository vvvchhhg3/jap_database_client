from PyQt5.QtWidgets import QWidget, QHBoxLayout

from low_level.item_name_editor_gui import ItemNameEditorGui
from low_level.list_editor_gui import ListEditorGui


class TabEditTagListGui(QWidget):
    list = None
    editor = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.list = ListEditorGui()
        self.editor = ItemNameEditorGui()
        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)
        l_main.addWidget(self.list, 1)
        l_main.addWidget(self.editor, 3)
