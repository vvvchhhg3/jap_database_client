from PyQt5.QtCore import pyqtSignal, QObject

from low_level.data_controller import DataController
from middle_level.word_editor_logic import WordEditorLogic
from middle_level.common_word_list_editor_logic import CommonWordListEditorLogic


class TabEditCommonWordListLogic(QObject):
    # Public methods.
    saved = pyqtSignal()

    def reshow(self):
        self.__editor.set_tags(DataController().tag_list.get_sorted_list())
        self.__list.set(DataController().common_word_list.get_sorted_list())

    # Private variables and constants.
    __list = None
    __editor = None

    # Private methods.
    def __init__(self, gui):
        super().__init__()
        # Init objects.
        self.__list = CommonWordListEditorLogic(gui.list)
        self.__editor = WordEditorLogic(gui.editor, DataController().common_word_list.is_word_contained)
        # Init connects.
        self.__list.selected.connect(self.__editor.set_word)
        self.__list.unselected.connect(self.__editor.reset_word)
        self.__list.saved.connect(self.__saved)
        self.__editor.replace.connect(self.__replace)
        # Start.
        self.reshow()

    # Private slots.
    def __saved(self):
        self.saved.emit()

    def __replace(self, old, new):
        DataController().common_word_list.replace_word(old, new)
        self.__list.set(DataController().common_word_list.get_sorted_list())
        self.__list.select(new)
        self.__list.update_gui_access()
        self.saved.emit()
