from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout

from low_level.funcs_gui import get_disabled_combobox, get_center_label, get_disabled_button
from middle_level.training_cards_gui import TrainingCardsGui


class TabTrainingCardsGui(QWidget):
    cb_list = None
    cb_num = None
    cb_type = None
    tc = None
    pb_start = None

    def __init__(self):
        super().__init__()
        # Init objects.
        self.cb_list = get_disabled_combobox()
        self.cb_num = get_disabled_combobox()
        self.cb_type = get_disabled_combobox()
        self.tc = TrainingCardsGui()
        self.pb_start = get_disabled_button("Start", 80)

        # Init GUI.
        l_main = QHBoxLayout(self)
        l_main.setContentsMargins(0, 0, 0, 0)

        l_cfg = QVBoxLayout()
        l_main.addLayout(l_cfg, 1)

        l_cfg.addWidget(get_center_label("List"))
        l_cfg.addWidget(self.cb_list)
        l_cfg.addWidget(get_center_label("Number"))
        l_cfg.addWidget(self.cb_num)
        l_cfg.addWidget(get_center_label("Type"))
        l_cfg.addWidget(self.cb_type)
        l_cfg.addStretch(1)
        l_cfg.addWidget(self.pb_start)

        l_main.addWidget(self.tc, 3)
