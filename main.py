import logging
import sys

from top_level.jap_database_client_logic import JapDatabaseClient

from PyQt5.QtWidgets import QApplication

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s: %(message)s')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    jdc = JapDatabaseClient()
    # app.focusChanged.connect(jdc.focus_changed)
    sys.exit(app.exec_())
